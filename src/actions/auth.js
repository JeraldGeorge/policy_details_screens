import * as actionTypes from '../constant/actionTypes';


//import { Router} from 'react-router-dom';

export function authenticate(data,routerHistory) {
    //console.log(routerHistory); 

   localStorage.setItem('Auth',data.userAuthKey);

    return async dispatch => {
    try {
    
        dispatch({
            type : actionTypes.AUTHENTICATE,
            user : data.userInfo
         });
        routerHistory.history.push('/dashboard');
}catch(ex){
    console.log(ex)
    routerHistory.history.push('/');
     }
 }
 };

export function logout(history)
{
    console.log(history)
  return  dispatch =>{
        dispatch({
               type : actionTypes.LOG_OUT,
      });
   localStorage.removeItem('Auth');
   localStorage.removeItem('persist:root');
   //localStorage.removeItem('persist:root')
   history.push('/');   
  }



};

export function networkIssue(data)
{
    return  dispatch =>{
     dispatch({
            type : actionTypes.NETWORK_ISSUE,
            errorPopup : data
         });
    

    }
   
}





 
