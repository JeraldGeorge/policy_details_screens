import * as actionTypes from '../constant/actionTypes';
import * as services from '../services/';

export function getPolicyDetails(){
    return async dispatch => {
        try{
            const data =  await services.getPolicyDetails();
            console.log('data',data);

            dispatch({
                type: actionTypes.DASHBOARD_DETAILS,
                policyDetail: data
            })
        }catch(err){
            console.log('err',err);
        }
    }
}