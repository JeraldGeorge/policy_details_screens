import * as actionTypes from '../constant/actionTypes';
import * as services from '../services/';



export function getDashboardDetails()
{
    return async dispatch => {
        try {
            const data=await services.getdashDetails();
            //console.log(data);
           if(!data.error)
           {
            dispatch({
                type : actionTypes.DASHBOARD_DETAILS,
                dashBoard : data
             });
           }else {
            dispatch({
                type : actionTypes.NETWORK_ISSUE,
                errorPopup : true
             });

           }
           

    }catch(ex){
        console.log(ex)
       
         }
     }

}