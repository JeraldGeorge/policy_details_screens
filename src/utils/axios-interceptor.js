import axios from "axios";
import setting from '../config/settings'

// LocalstorageService
//const localStorageService = localStorage.getItem('token') ? localStorage.getItem('token') : '' ;
const API_URL = (process.env.NODE_ENV !== 'production') ? setting.LOCAL_API_URL : setting.DEV_API_URL;
// Add a request interceptor
const token=localStorage.getItem('Auth') || '';
axios.defaults.baseURL = API_URL;
axios.defaults.headers.common = {'Authorization': `Bearer ${token}`}


  

export default axios; 