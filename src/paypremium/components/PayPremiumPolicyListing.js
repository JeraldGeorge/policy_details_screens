import React, { useEffect, useState } from "react";
import { isMobile } from "react-device-detect";
// import Header from "../components/Header";
// import Footer from "../components/Footer";
import SideNav from "../../home/containers/sideNav";
// import FloatingMenu from "../containers/FloatingMenu";
import Loader from "../../home/containers/loader";
import { NavLink } from "react-router-dom";
import QuickActions from "../containers/QuickActions";
import PayPremiumPolicyCard from "../containers/PayPremiumPolicyCard";
import PayPremiumPolicyCard1 from "../containers/PayPremiumPolicyCard1";

function PayPremiumPolicyListing() {
    const [loader, setLoader] = useState(true);
    const [sidebarMobileState, setSidebarMobileState] = useState(false);
    const [modalPleaseNoteState, setModalPleaseNoteState] = useState(true);

    useEffect(() => {
        setLoader(false);
    }, []);


    const sideBarHandlerMobile = () => {
        if (sidebarMobileState) {
            setSidebarMobileState(false);
        } else {
            setSidebarMobileState(true);
        }
    };
    const modalPleaseNoteHandler = ()=>{
        setModalPleaseNoteState(false);
    }
    return (
        <>
            {/* <Header /> */}
            <div className="container-fluid refContainer">
                <SideNav openStateMobile={sidebarMobileState} />
                <div className="content-wrapper" >
                    <div className="lytPaymentPolicyListing">
                        {/* action header start */}
                        <div className="bs-action-header">
                            {isMobile ? (
                                <h1 className="title" onClick={sideBarHandlerMobile}>
                                    Pay Premium
                                    <span className="icon icon-chevron-down"></span>
                                </h1>
                            ) : (
                                    <h1 className="title">
                                        Pay Premium
                                        <span className="icon icon-chevron-down"></span>
                                    </h1>
                                )}
                            <QuickActions />
                        </div>
                        {/* action header end */}
                        {/* breadcrumb starts */}
                        <ol className="m-breadcrumb desktop">
                            <li className="item">
                                <NavLink to="" className="link" title="Home">
                                    Home<span className="icon icon-chevron-right"></span>
                                </NavLink>
                            </li>
                            <li className="item">Pay Premium</li>
                        </ol>
                        {/* breadcrumb end */}
                        {/* page template start here */}
                        <div className="bs-section">
                            <div className="sec-head">
                                <h2 className="sec-title">Pay Renewal Premium</h2>
                                <h3 className="sec-subtitle">Renew your policies on time and secure your future</h3>
                            </div>
                            <div className="sec-cont">
                                <PayPremiumPolicyCard />
                                <PayPremiumPolicyCard1 />
                                <div className="act-btn  text-right">
                                    <button className="btn btn-fixed">Continue</button>
                                </div>
                            </div>
                        </div>
                        {/* page template end here */}
                    </div>
                </div>
            </div>
            {/* <Footer /> */}
            {/* popup */}
            <div className={`modal ${(modalPleaseNoteState) ? 'active' : ''}`}>
                <div className="dialog">
                    <div className="content">
                        <div className="modal-header">
                            <h5 className="title">Please Note</h5>
                            <button type="button" onClick={modalPleaseNoteHandler} className="btn btn-icon">
                                <span className="icon icon-cancel"></span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p className="para">You have opted for NACH as your payment method. Irrespective of your online premium payment your NACH transaction will be processed as scheduled. Any excess premium received will be refunded in 3 working days.</p>
                        </div>
                        <div className="act-btn">
                            <button type="button" className="btn">Continue</button>
                        </div>
                    </div>
                </div>
            </div>
            <span className={`overlay ${(modalPleaseNoteState)?'active':''}`}></span>
            {/* <FloatingMenu /> */}
            <Loader show={loader} />

        </>
    );
}

export default PayPremiumPolicyListing;
