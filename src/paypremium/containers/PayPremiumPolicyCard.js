import React, { useState, useEffect } from 'react';
// import InfoHelp from './InfoHelp';

function PayPremiumPolicyCard() {
    const [payPolicyCardState, setPayPolicyCardState] = useState(true);
    const [showEditableFld, setShowEditableFld] = useState(false);

    useEffect(() => {

    })
    const payPolicyCardStateHandler = () => {
        setPayPolicyCardState(!payPolicyCardState)
    }
    const customisedAmountHandler = (e) => {
        console.log(e.currentTarget.checked)
        if (e.currentTarget.value == 'customValue') {
            setShowEditableFld(true)
        } else {
            setShowEditableFld(false)
        }

    }
    return (
        <>
            <div className={`m-policy-card ${(payPolicyCardState) ? 'expanded' : ''}`}>
                <div className="head" onClick={payPolicyCardStateHandler}>
                    <div className="row">
                        <div className="col-sm-6">
                            <div className="col-sm-5">
                                <div className="m-lbl-text">
                                    <label className="lbl-text">Policy No.</label>
                                    <span className="desc-text emp">726183618</span>
                                </div>
                            </div>
                            <div className="col-sm-7">
                                <div className="m-lbl-text">
                                    <label className="lbl-text">Policy Type</label>
                                    <span className="desc-text">Unit Linked Insurance Policy</span>
                                </div>
                            </div>
                        </div>
                        <div className="extra col-sm-6">
                            
                            <div className="col-sm-6">
                                <div className="m-lbl-text">
                                    <label className="lbl-text">Total premium</label>
                                    <span className="desc-text">₹ 6,000</span>
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="m-lbl-text">
                                    <label className="lbl-text">Premium due on</label>
                                    <span className="desc-text">25 Mar 2020</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="checkbox white">
                        <input type="checkbox" className="checkbox__input" id="paypolicy1" />
                        <label className="checkbox-lbl-text" htmlFor="paypolicy1"></label>
                    </div>
                    <button className="btn btn-icon" ><span className="icon icon-chevron-down"></span></button>

                </div>
                <div className="body">
                    <div className="extra row">
                        <div className="col-xs-6">
                            <div className="m-lbl-text">
                                <label className="lbl-text">Premium due on</label>
                                <span className="desc-text">25 Mar 2020</span>
                            </div>
                        </div>
                        <div className="col-xs-6">
                            <div className="m-lbl-text ">
                                <label className="lbl-text">Total premium</label>
                                <span className="desc-text">₹ 6,000</span>
                            </div>
                        </div>
                    </div>
                    <div className="content">
                        <div className="msg">Premium payment due by 5 Apr 2020</div>
                        <div className="row">
                            <div className="col-sm-6 col-xs-7">
                                <div className="radio">
                                    <input type="radio" className="radio__input" name="grp1" id="radio1" onChange={(e) => { customisedAmountHandler(e) }} />
                                    <label className="radio-lbl" htmlFor="radio1">Total premium due <span className="innertext">Incl. due in next 30 days</span></label>
                                </div>
                            </div>
                            <div className="col-sm-3 col-xs-5">
                                <div className="m-lbl-text">
                                    <label className="lbl-text desktop">Premium</label>
                                    <div className="desc-text">
                                        ₹ 10,000
                                        {/* <InfoHelp /> */}
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-3 col-xs-12">
                                <div className="m-lbl-text">
                                    <label className="lbl-text desktop">Due on</label>
                                    <span className="desc-text">05 Jun 2020</span>
                                </div>
                            </div>
                            
                        </div>
                        <div className="row">
                            <div className="col-sm-6 col-xs-7">
                                <div className="radio">
                                    <input type="radio" className="radio__input" name="grp1" id="radio2" onChange={(e) => { customisedAmountHandler(e) }} />
                                    <label className="radio-lbl" htmlFor="radio2">Total premium due as of today</label>
                                </div>
                            </div>
                            <div className="col-sm-3 col-xs-5">
                                <div className="m-lbl-text">
                                    <label className="lbl-text desktop">Premium</label>
                                    <div className="desc-text">
                                        ₹ 10,000
                                        {/* <InfoHelp /> */}
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-3 col-xs-12 ">
                                <div className="m-lbl-text">
                                    <label className="lbl-text desktop">Due on</label>
                                    <span className="desc-text">05 Jun 2020</span>
                                </div>
                            </div>
                            
                        </div>
                        <div className="row">
                            <div className="col-sm-6 col-xs-7">
                                <div className="m-lbl-text">
                                    <span className="desc-text">Advance premium paid</span>
                                </div>
                            </div>
                            <div className="col-sm-3 col-xs-5">
                                <div className="m-lbl-text">
                                    <label className="lbl-text desktop">Premium</label>
                                    <span className="desc-text">₹ 10,000 </span>
                                </div>
                            </div>
                            <div className="col-sm-3 col-xs-12">
                                <div className="m-lbl-text">
                                    <label className="lbl-text desktop">Paid On</label>
                                    <span className="desc-text">05 Feb 2020</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default PayPremiumPolicyCard
