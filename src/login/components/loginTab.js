import React, { useState,useEffect } from "react";
import { NavLink } from "react-router-dom";
import Loader from "../../home/containers/loader";
import CreateId from '../containers/createId';
import { abcLogin } from '../../services'
import { otpLogin,verifyOtp } from '../../services'
import OTP from '../containers/otp';

import { useForm } from "react-hook-form";


const LoginTab = props => { 
  
  
  const [loader,setLoader] = useState(true);
  const [tab, setTab] = useState("tab1");
  const [pwdShowState, setPwdShowState] = useState(false);
  //const [passwordError,setPassworderror]=useState(false);
  const [errorMessage,seterrorMessage]=useState('');
  const [otpTab,setOtpTab]=useState(false);
  //const abcId=props.abcId;
  const [otpData,setotpData]=useState('');
  const [otpError,setotpError]=useState('');
  const [otpReference,setotpReference]=useState('');
  const [submitAbc,setSubmit]=useState('');
  const { register, errors, handleSubmit} = useForm({
    defaultValues : {userId : props.abcId   }, 
    mode: "onBlur"
  });

  const {
    register: register2,
    errors: errors2,
    handleSubmit: handleSubmit2
  } = useForm({
    defaultValues : {abcId : props.abcId   }, 
    mode: "onBlur"
  });


  useEffect(()=>{
    setLoader(false);
   // reset({userId : props.abcId})
  },[])

  const goBack = event =>{
      event.preventDefault();
      props.goBackfn();
  }

  const handleuserSubmit = async(data) => {
    setLoader(true);
    const postData={"api" : "loginWithPassword","userName":data.userId ,"password" : data.password};
    const postResponse = await abcLogin(postData);
    if(postResponse.data && (postResponse.data.ReturnCode==="2" || postResponse.data.ReturnCode==="4"))
    {
      setLoader(false);
      seterrorMessage(postResponse.data.ReturMessage);
    
    }else if(postResponse.data && postResponse.data.ReturnCode==="1")
    {
      setLoader(false);
      const history={"history":props.history};
      props.authenticate(postResponse.data,history); 
    //   const postref=postResponse.data;
    //  fetch('https://login.abcscstg.com/interface/', { 
    //     mode: "no-cors",
    //     method: 'post', 
    //     headers: new Headers({
    //       'Content-Type': 'application/x-www-form-urlencoded',
    //      // 'Access-Control-Allow-Origin':'*'
    //     }), 
    //     body:`TokenNo=${postref.TokenNo}&TokenRefNo=${postref.TokenRefNo}&CIINumber=${postref.userInfo.CIINumber}`
    //   }).catch(err => {
    //     setLoader(false);
    //     props.networkIssue(true);
    //   });
   
} else if(postResponse.error){
      setLoader(false); 
      props.networkIssue(true);
  }else{
   setLoader(false); 
  }
}

  const sendOtp = async(data) =>{

     setLoader(true);
     setSubmit(data.abcId);
      const postData={"api" : "sendOTP","functionality":"OTP_LOGIN","userId":data.abcId};
      const callService=await otpLogin(postData);
      if(callService && callService.data)
      {
        
        setotpData(callService.message);
        setotpReference(callService.data.insertedId);
        setOtpTab(true);
        setLoader(false);
      }
      else if(callService.error){
        setLoader(false); 
        props.networkIssue(true);
    }else{
     setLoader(false); 
    }


}



  const chkotpHandler = async(otpData)  => {
  
  //console.log(otpData);
  setLoader(true);
  const postData={"api":"verifyOTP","functionality":"OTP_LOGIN","otpReferenceId":otpReference,"otp":otpData };
  const responseData = await verifyOtp(postData);
  if(responseData.status===10 && responseData.data)
  {
    setLoader(false);
    const history={"history":props.history};
    props.authenticate(responseData.data,history); 
    
  }else if(responseData.status===20){
    setotpError(responseData.message);
    setLoader(false); 
     
  } else if(responseData.error){
    setLoader(false); 
    props.networkIssue(true);
}else{
 setLoader(false); 
}
  
}
const setTabHandler = (e, param) => {
    e.preventDefault();
    switch (param) {
      case 'tab1':
        setTab('tab1');
        break;
      case 'tab2':
        setTab('tab2');
        break;
      default :
       setTab('tab1');  
    }
  }
  
  const pwdShowStateHandler = () => {
    setPwdShowState(!pwdShowState);
  }
  
  //Otp Handling Function
  
  const setOtpErrorStatus = data =>{
    setotpError(data);
  }
  
  const eventgoBack = () => {
    setOtpTab(false);
  }


  const setEmptyerror = () => {
       setotpError('');
  }
  const reSend = async() => {
   // console.log('prasad')
    const postData={"api" : "sendOTP","functionality":"OTP_LOGIN","userId":submitAbc};
    const callService=await otpLogin(postData);
    if(callService && callService.data)
    {
      
      setotpData(callService.message);
      setotpReference(callService.data.insertedId);
      //setOtpTab(true);
      setLoader(false);
    }
    else if(callService.error){
      setLoader(false); 
      props.networkIssue(true);
  }else{
   setLoader(false); 
  }
  }

  
  return (
     <>
      {!otpTab  ?
      <div className="container-fluid">
        <div className="lyt-login">
          <div className="lhs">
            <NavLink className="btnBack" to="" onClick={ goBack.bind(this)} title="Go Back">
              <span className="icon icon-arrow-left"></span>
              <span className="text">Go Back</span>
            </NavLink>
            <div className="m-login-journey">
              <div className="head">
                <h1 className="title">Login</h1>
                <label className="lbl">Enter your information below to continue</label>
              </div>
              <div className="cont card">
                  <div className="tab">
                    <ul className="tab-nav">
                      <li className={`nav-btn ${(tab === 'tab1') ? 'active' : ''}`}>
                        <NavLink to="#tab1" title="Login with password" onClick={(e) => { setTabHandler(e, 'tab1') }}>Login with password</NavLink>
                      </li>
                      <li className={`nav-btn ${(tab === 'tab2') ? 'active' : ''}`}>
                        <NavLink to="#tab2" title="Login with OTP" onClick={(e) => { setTabHandler(e, 'tab2') }}>Login with OTP</NavLink>
                      </li>
                    </ul>
               <div className="tab-content">
                      <div id="tab1" className={`tab-panel ${(tab === 'tab1') ? 'active' : ''}`}>
                        <form key={1} onSubmit={handleSubmit(handleuserSubmit)}>
                        <div className={errors && errors.userId ? 'input-group show-error' : 'input-group'}>
                          <label className="lbl">One ABC ID</label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Enter your User ID"
                            name ="userId" 
                            autoComplete="off"
                            // ref={register({ required: true })}
                           ref={register({
                            required: "Please Enter ABC ID",
                            maxLength:50,
                            pattern: {
                              value: /^[a-zA-Z0-9_.-]*$/,
                              message: "Invalid User Id"
                            }
                          })}
                          />
                          <div className="input-group-append">
                            {/* <NavLink to="/forgotUser" className="btn btn-link" title="Forgot User ID?">Forgot User ID?</NavLink> */}
                          </div>
                          <span className="error">
                          {(errors.userId && errors.userId.message) ? errors.userId.message : ''} 
                            </span>
                        </div>
                        <div className={ (errors && errors.password) || errorMessage ? 'input-group pass show-error' : 'input-group pass'}>
                          <label className="lbl">Password</label>
                          <div className="addon">
                            <input
                              type={`${(pwdShowState) ? 'text' : 'password'}`}
                              className="form-control"
                              placeholder="Enter Password"
                              name="password"
                              maxLength={50}
                              autoComplete="off"
                              ref={register({ required: true })}
                            />

                            <button className="btn" onClick={pwdShowStateHandler} type="button">
                              <span className={`icon ${(pwdShowState) ? 'icon-no-view' : 'icon-view'}`}></span>
                            </button>
                          </div>
                          <div className="input-group-append">
                            <NavLink to="/forgotPassword" className="btn btn-link" title="Forgot password?">Forgot password?</NavLink>
                          </div>
                          <span className="error">
                          {  errorMessage ? errorMessage : 'Password is required'}
                            </span>
                        </div>
                        <div className="act-btn">
                          <button 
                          className="btn btn-link" 
                          type="button" 
                          onClick = {(e)=>{
                            e.preventDefault();
                            props.history.push('/register')
                        } } 
                          >
                            Register now
                                </button>
                          <button className="btn" type="submit">
                            Submit
                           </button>
                        </div>
                        </form>
                     </div>
                      <div id="tab2" className={`tab-panel ${(tab === 'tab2') ? 'active' : ''}`}>
                        <form key={2} onSubmit={handleSubmit2(sendOtp)}>
                        <div className={errors2 && errors2.abcId ? 'input-group show-error' : 'input-group'}>
                          <label className="lbl">One ABC ID</label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Enter your User ID"
                            name="abcId"
                            autoComplete="off"
                            ref={register2({
                              required: "Please Enter AbCID",
                              pattern: {
                                value: /^[a-zA-Z0-9_.-]*$/,
                                message: "Invalid User Id"
                              }
                            })} 
                          />
                          <div className="input-group-append">
                            <NavLink to="/forgotUser" className="btn btn-link" title="Forgot User ID?">Forgot User ID?</NavLink>
                          </div>
                          <span className="error">
                          {(errors.abcId && errors.abcId.message) ? errors.abcId.message : ''} 
                             </span>
                        </div>
                        <div className="act-btn">
                          <button className="btn" type="submit">
                            Send OTP
                          </button>
                        </div>
                      </form>
                      </div>
                    </div>
                  </div>
              
              </div>
            </div>
          </div>
          <div className="rhs">
            <CreateId/>
          </div>
        </div>
      </div>:  
      <OTP 
      maskedData={ otpData }  
      otpData={ chkotpHandler } 
      otpError={ otpError } 
      otpErrorStatus={ setOtpErrorStatus }
      goBackevent={ eventgoBack }
      setEmptyerror={ setEmptyerror }
      reSend={ reSend }
      /> }
        <Loader show={loader} />
     </>
  );
};

export default LoginTab;
