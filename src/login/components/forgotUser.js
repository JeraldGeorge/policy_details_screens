import React, { useState,useEffect } from "react";
import DateFnsUtils from "@date-io/date-fns";
import Loader from "../../home/containers/loader";
import CreateId from '../../login/containers/createId';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from "@material-ui/pickers";
import { NavLink } from "react-router-dom";
import LoginPop from '../containers/loginPop';
import OTP from '../containers/otp';
import { useForm } from "react-hook-form";
import moment from 'moment';
import { sendOtp,verifyOtp } from '../../services'


const ForgotUser = props => {
  const { register, errors, handleSubmit } = useForm({
    mode: "onBlur"
  });
  const [loader,setLoader] = useState(true);
  const [selectedDate, setSelectedDate] = useState(null);
  const [ABCidModalState,setABCidModalState] = useState(false);
  const [forgotUser,setforgotUser]=useState(true);
  const [errorPolicydate,seterrorPolicydate]=useState(false);
      // Value TO be passed to OTP Component
  const [otpData,setotpData]=useState({email : '', mobile:''});
  const [otpError,setotpError]=useState(false);
  const [otpReference,setotpReference]=useState('');
  const [submitPolicy,setsubmitPolicy]=useState('');
  
   


  useEffect(()=>{
    setLoader(false);
  },[])

  const handleDateChange = (date) => {
    seterrorPolicydate(false);
    setSelectedDate(date);
  };
  const closeModalHandler = ()=>{
    setABCidModalState(false);
  }
  const chkotpHandler = async(otpData)  => {
    setLoader(true);
    const postData={"api" : "verifyOTP","otpReferenceId":otpReference,"otp" :otpData};
    const verify= await verifyOtp(postData);
    if(verify.data && verify.status===10)
    {
      setLoader(false);
      setABCidModalState(true);
    }else{
      setLoader(false);
    }
    
    // if(otpData === otp)
    // {
    //   setABCidModalState(true);
    // }else {
    //   setotpError(true);
    //  }
  }
  
  const setOtpErrorStatus = data =>{
    setotpError(data);
  }
  
  const eventgoBack = () => {
    //console.log('prasad');
    setSelectedDate(null);
    seterrorPolicydate(false);
    setforgotUser(true);
  }

  const goLogin = event => {
    event.preventDefault();
    props.history.push('/');

  }
  
  const onSubmit = async(data) => {
    if(selectedDate===null)
    {
      seterrorPolicydate(true);
      return;  
    }
    setLoader(true);
    const formatDate=moment(selectedDate).format("DD/MM/YYYY");
    setsubmitPolicy(data.policyNo)
    const reqData={"policyNumber" : data.policyNo,"dob" : formatDate,"api":"sendOTP","functionality":"OTP_FORG_USR" }
    const reqOtp=await sendOtp(reqData);
   if(reqOtp.data && reqOtp.status===10)
   {
     setLoader(false);
     const otpData={message:reqOtp.message };
     const otpReference={otpReference : reqOtp.data.insertedId }
     setotpReference(otpReference);
     setotpData(otpData); 
     setforgotUser(false);
  }else{
    setLoader(false);
  
  }
}

const setEmptyerror = () => {
  setotpError('');
}
const reSend = async() => {
// console.log('prasad')
const formatDate=moment(selectedDate).format("DD/MM/YYYY");
const reqData={policyNumber :submitPolicy,dob : formatDate,api:"sendOTP","functionality":"OTP_FORG_USR" }
const reqOtp=await sendOtp(reqData);
if(reqOtp && reqOtp.data)
{
 
 setotpData(reqOtp.message);
 setotpReference(reqOtp.data.insertedId);
 //setOtpTab(true);
 setLoader(false);
}
else if(reqOtp.error){
 setLoader(false); 
 props.networkIssue(true);
}else{
setLoader(false); 
}
}


  return (
    <>
      
    { forgotUser ?
    <div className="container-fluid">
    <div className="lyt-login">
      <div className="lhs">
        <NavLink className="btnBack" to="/" title="Go Back">
          <span className="icon icon-arrow-left"></span>
          <span className="text">Go Back</span>
        </NavLink>
        <div className="m-login-journey">
          <div className="head">
            <h1 className="title">Forgot User ID</h1>
            <label className="lbl">
              Enter your information below to continue
            </label>
          </div>
          <div className="cont card">
            <form onSubmit={ handleSubmit(onSubmit) }>
              <div className={errors.policyNo  ? "form-group show-error" : "form-group" }>
                <label className="lbl">Policy No./Application No.</label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Enter Policy No"
                  name="policyNo"
                  ref={register({
                    required: "Please Enter Policy No./Application No",
                    maxLength:10,
                    pattern: {
                      value: /^[a-zA-Z0-9]{3}[0-9]{7}$/,
                      message: "Invalid Policy No./Application No"
                    }
                  })}
                />
                <span className="error">
                {(errors.policyNo && errors.policyNo.message) && errors.policyNo.message}
                 {(errors.policyNo && errors.policyNo.type==="maxLength") && 'Maximum Length Exceeded'}
                </span>
              </div>
              <div className="form-group">
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    autoOk
                    disableFuture
                    inputVariant="standard"
                    openTo="year"
                    format="dd/MM/yyyy"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    label="Policy Owner Date of Birth (DD/MM/YYYY)"
                    placeholder="DD/MM/YYYY"
                    views={["year", "month", "date"]}
                    value={selectedDate}
                    onChange={handleDateChange}
                    keyboardIcon={''}
                    helperText={''}
                  />
                </MuiPickersUtilsProvider>
                <span className="error">
                  Please Fill the Date
                </span>
              </div>
              <div className="act-btn">
                <button className={ !errors ? "btn btn--disabled" : "btn"} type="submit">
                  Send OTP
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div className="rhs">
        <CreateId/>
      </div>
    </div>
  </div> 
    : 
  <OTP 
      maskedData={ otpData }   
      otpData={ chkotpHandler } 
      otpError={ otpError } 
      otpErrorStatus={ setOtpErrorStatus }
      goBackevent={ eventgoBack }
      setEmptyerror={ setEmptyerror }
      reSend={ reSend }
      /> }
            <LoginPop 
            closeModalHandler={closeModalHandler} 
            ABCidModalState={ ABCidModalState } 
            goLogin={ goLogin }
            />
     
      <Loader show={loader} />
    </>
  );
};

export default ForgotUser;
