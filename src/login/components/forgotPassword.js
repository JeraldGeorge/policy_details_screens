import React, { useState, useEffect } from "react";
import { connect } from 'react-redux';
import DateFnsUtils from "@date-io/date-fns";
import Loader from "../../home/containers/loader";
import CreateId from '../../login/containers/createId';
import {
  MuiPickersUtilsProvider,
  //KeyboardTimePicker,
  //DatePicker,
  KeyboardDatePicker
} from "@material-ui/pickers";
import { NavLink } from "react-router-dom";
import OTP from '../containers/otp';
import { convertFormdata } from '../../utils/helper-function';
import { networkIssue } from '../../actions/auth'
import { useForm } from "react-hook-form";
import { sendOtp,verifyOtp } from '../../services'
import moment from 'moment';


const ForgotPassword = props => {
  const { register, errors, handleSubmit } = useForm({
    mode: "onBlur"
  });
  const [loader,setLoader] = useState(true);
  const [selectedDate, setSelectedDate] = useState(null);
 // const [forgotPassword,setforgotPassword] =useState(true);
  //const [enteredabcID,setabcId]=useState('');
  //const [errorAbcid,seterrorAbcid]=useState(false);
  const [errorDob,seterrorDob] =useState(false);
  //OTP Props to be Passed 
  const [otpTab,setotpTab] = useState(false);
  const [otpData,setotpData]=useState('');
  const [otpError,setotpError]=useState(false);
  const [otpReference,setotpReference]=useState('');
  const [submitPassDetail,setsubmitpassDetail]=useState('');
  const [invalidMessage,setInvalidmessage]=useState('');
  


  useEffect(()=>{
    //console.log(otpTab)
    setLoader(false);
  },[])
  const handleDateChange = (date) => {
    seterrorDob(false);
    setSelectedDate(date);
  };

  const chkotpHandler = async(otpData)  => {
    setLoader(true);
    const postData={"api":"verifyOTP","functionality":"OTP_FORG_PWD","otpReferenceId":otpReference,"otp":otpData };
    const responseData = await verifyOtp(postData);
    if(responseData && responseData.status===10 && responseData.data)
    {
      //setLoader(false);
      const postData = convertFormdata(responseData.data);
      //console.log(postData)
      fetch(responseData.data.redirectUrl, { 
        mode: "no-cors",
        method: 'post', 
        headers: new Headers({
          'Content-Type': 'application/x-www-form-urlencoded',
          'Access-Control-Allow-Origin':'*'
        }), 
        body:postData,
        //processBody:false
      }).catch(err => {
        props.networkIssue(true);
    });
  }else if(responseData && (responseData.status===20 || responseData.status===21)){
   
      setotpError(responseData.message);
      setLoader(false); 
       
 } else if(responseData && responseData.error){
      setLoader(false); 
      props.networkIssue(true);
  }else{
  
   setLoader(false); 
  }
 }
  
  

  const onSubmit = async(data) => {
      //event.preventDefault();
      setLoader(true)
      if(selectedDate===null)
      {
        seterrorDob(true);
        return;  
      }
      const formatDate=moment(selectedDate).format("DD/MM/YYYY");
      const reqData={'api':'sendOTP','functionality':'OTP_FORG_PWD','userId' :data.abcId,'dob': formatDate };
      const serviceRequest=await sendOtp(reqData);
      if(serviceRequest && serviceRequest.data)
      {
       setLoader(false)
       setotpReference(serviceRequest.data.insertedId);
       setsubmitpassDetail(reqData);
       setotpData(serviceRequest.message);
       setotpTab(true);

      }else if(serviceRequest &&  serviceRequest.status===21)
      {
        setLoader(false)
        setInvalidmessage(serviceRequest.message);
      }if(serviceRequest && serviceRequest.error)
      { 
       setLoader(false);
        props.networkIssue(true);
      }else{
       
        setLoader(false);
      }
  }

  //OTP Handler 
  const setOtpErrorStatus = data =>{
    setotpError(data);
  }

   const eventgoBack = () => {
    // console.log("evenr")
    setotpTab(false);
    setSelectedDate(null);
    seterrorDob(false);
  }
 
  
  const setEmptyerror = () => {
       setotpError('');
  }
  const reSend = async() => {
    const reqData=submitPassDetail;
    const serviceRequest=await sendOtp(reqData);
    if(serviceRequest && serviceRequest.data)
    {
     setLoader(false)
     setotpReference(serviceRequest.data.insertedId);
     setsubmitpassDetail(reqData);
     setotpData(serviceRequest.message);
     setotpTab(true);

    }else if(serviceRequest &&  serviceRequest.status===21)
    {
      setLoader(false)
      setotpError(serviceRequest.message);
    }if(serviceRequest && serviceRequest.error)
    { 
     setLoader(false);
      props.networkIssue(true);
    }else{
     
      setLoader(false);
    }
    
  }




  return (
    <>
      { !otpTab ? 
      <div className="container-fluid">
      <div className="lyt-login">
        <div className="lhs">
          <NavLink className="btnBack" to="/" title="Go Back">
            <span className="icon icon-arrow-left"></span>
            <span className="text">Go Back</span>
          </NavLink>
          <div className="m-login-journey">
            <div className="head">
              <h1 className="title">Forgot Password</h1>
              <label className="lbl">
                Enter your information below to continue
              </label>
            </div>
            <div className="cont card">
              <form onSubmit={ handleSubmit(onSubmit) }>
                <div className={invalidMessage || errors.abcId ? "form-group show-error" : "form-group"}>
                  <label className="lbl">One ABC ID</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Enter ABC Id"
                    name="abcId"
                    ref={register({
                      required: "Please Enter ABC ID",
                      maxLength:50,
                      pattern: {
                        value: /^[a-zA-Z0-9_.-]*$/,
                        message: "Invalid ABC Id"
                      }
                    })}
                  />
                  <span className="error">
                  { invalidMessage &&  'ABC ID is required' }
                  {(errors.abcId && errors.abcId.message) && errors.abcId.message}
                 {(errors.abcId && errors.abcId.type==="maxLength") && 'Maximum Length Exceeded'}

                  </span>
                </div>
                <div className={ errorDob ? "form-group show-error" : "form-group"}>
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                      autoOk
                      disableFuture
                      inputVariant="standard"
                      openTo="year"
                      format="dd/MM/yyyy"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      label="Policy Owner Date of Birth (DD/MM/YYYY)"
                      placeholder="DD/MM/YYYY"
                      views={["year", "month", "date"]}
                      value={selectedDate}
                      onChange={handleDateChange}
                      keyboardIcon={''}
                      helperText={''}
                    />
                  </MuiPickersUtilsProvider>
                  <span className="error">
                    Please check the Policy No./Application No. and try again
                  </span>
                </div>
                <div className="act-btn">
                  <button className="btn" type="submit">
                    Send OTP
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div className="rhs">
          <CreateId/>
        </div>
      </div>
    </div>
    
       :   
       <OTP 
       maskedData={ otpData }  
       otpData={ chkotpHandler } 
       otpError={ otpError } 
       otpErrorStatus={ setOtpErrorStatus }
       goBackevent={ eventgoBack }
       setEmptyerror={ setEmptyerror }
       reSend={ reSend }
       
       />
       }
      <Loader show={loader} />
    </>
  );
};

const mapDispatchToProps = dispatch => {
  return {
      networkIssue : data =>dispatch(networkIssue(data))
   };
};
export default connect(null,mapDispatchToProps)(ForgotPassword);
