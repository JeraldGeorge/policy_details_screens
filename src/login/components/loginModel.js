import React from 'react';

const LoginModal = props =>{

return (
 <>
 <div className={`modal ${(props.loginModalState)?'modal--active':''}`}>
        <div className="modal__dialog">
          <div className="modal__content">
            <div className="modal__header">
              <h5 className="modal__title">You have an existing ABSLI ID</h5>
              <button type="button" onClick={props.closeLoginModal} className="btn btn--icon">
                <span className="icon icon-cancel"></span>
              </button>
            </div>
            <div className="modal__body">
              <p className="modal__para">
                We have migrated to a new portal to provide you with an enhanced
                experience across the entire Aditya Birla Capital portfolio.
                Please register for one ABC ID to experience it.
              </p>
              <div className="modal__highlight">
                <p>
                  We have migrated to a new portal to provide you with an
                  enhanced experience across the entire Aditya Birla Capital
                  portfolio. Please register for one ABC ID to experience it.
                </p>
              </div>
            </div>
            <div className="actBtn actBtn--col2">
              <button type="button" className="btn btn--outline">
                Continue with existing ID
              </button>
              <button type="button" className="btn">
                Register for One ABC ID
              </button>
            </div>
          </div>
        </div>
      </div>
      <span className={`overlay ${(props.loginModalState)?'active':''}`}></span>
 
 </>

);

}
export default LoginModal;
