import React,{ useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import CreateId from '../containers/createId';
import Loader from '../../home/containers/loader';
import { useForm } from 'react-hook-form';
import { chkUser } from '../../services';

const LoginUserId = props => {
  
  const { register, triggerValidation, errors,getValues } = useForm({
    mode: "onBlur"
  });
  
  const [pwdShowState,setPwdShowState] = useState(false);
  const [usersErrorMessage,setusersErrorMessage ]=useState('');
  const [userErrorMessage ,setUserMessage]  = useState(false); 
  const [enablePasswordInput,setPasswordInput] = useState(false);
  const [passwordError,setpasswordError]=useState(false);
  const [loader,setLoader] = useState(true);
  useEffect(()=>{
    setLoader(false);
  },[]) 

  
    const chkUserId = async(user)  => {
       setLoader(true);
       const checkUser= await chkUser(user);
       
       if(checkUser.data && checkUser.data.ReturnCode === "1" && checkUser.data.isOneABC)
       {
          setLoader(false);
          props.handleLogin(checkUser.data.ABCUserID); 
       }else if(checkUser.data && checkUser.data.ReturnCode === "1" && !checkUser.data.isOneABC)
       {
              setLoader(false);
              setPasswordInput(true);
       }
       else if(checkUser.data && checkUser.data.ReturnCode==="0"){
         setLoader(false); 
        setusersErrorMessage(checkUser.data.ReturMessage);
        setUserMessage(true);
     }
     else if(checkUser.error){
         setLoader(false); 
         props.networkIssue(true);
     }else{
      setLoader(false); 
     }
 }
   
   
   const submitData = userData => {
    props.handleLogin(userData);
  }


  // const closeLoginModal = ()=>{
  //   setLoginModalState(false);
  // }
  const pwdShowStateHandler = ()=>{
      
    setPwdShowState(!pwdShowState);
  }
  return (
    <>
     <div className="container-fluid">
        <div className="lyt-login">
          <div className="lhs">
            <NavLink className="btnBack" to="" title="Go Back">
              {/* <span className="icon icon-arrow-left"></span> */}
              <span className="text"></span>
            </NavLink>
            <div className="m-login-journey">
              <div className="head">
                <h1 className="title">Login</h1>
                <label className="lbl">
                  Enter your information below to continue
                </label>
              </div>
              <div className="cont card">
                <form>
                <div className={(errors && errors.userId) || userErrorMessage  ? 'input-group show-error' : 'input-group'}>
                    <label className="lbl">User ID</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Enter your User ID"
                      name="userId"
                      autoComplete="off"
                      //maxLength={50}
                      ref={register({
                        required: "Please Enter UserID",
                        maxLength:50,
                        pattern: {
                          value: /^[a-zA-Z0-9_.-]*$/,
                          message: "Invalid User Id"
                        }
                      })}
                     />
                    <div className="input-group-append">
                      <NavLink className="btn btn-link" to="/forgotUser" title="Forgot User ID?">Forgot User ID?</NavLink>
                    </div>
                    <span className="error">
                    { usersErrorMessage && usersErrorMessage }
                    {(errors.userId && errors.userId.message) ? errors.userId.message : ''} 
                    {(errors.userId && errors.userId.type==="maxLength") && 'Maximum Length Exceeded'}
                    </span> 
                  </div>
                 
                  { enablePasswordInput &&   <div className={ errors.password ? 'input-group pass show-error' : 'input-group pass' }>
                    <label className="lbl">Password</label>
                    <div className="addon">
                      <input
                        type={`${(pwdShowState)?'text':'password'}`}
                        className="form-control"
                        placeholder="Enter Password"
                        name ="password"
                        maxLength={50}
                        autoComplete="off"
                        ref={register(
                          { required: true }
                        )} 
                      />
                      <button className="btn" onClick={pwdShowStateHandler} type="button">
                        <span className={`icon ${(pwdShowState)?'icon-no-view':'icon-view'}`}></span>
                      </button>
                    </div>
                    <div className="input-group-append">
                      <NavLink className="btn btn-link" to="/forgotPassword" title="Forgot password?">Forgot password?</NavLink>
                    </div>
                    <span className="error">
                      Your password in incorrect. Please try again.
                    </span>
                      </div>  }
                  <div className="act-btn">
                    <button 
                    className="btn btn-link"
                    type="button"
                    onClick={ (e) => {
                      e.preventDefault();
                      props.history.push('/register')
                    } } 
                    >
                      Register now
                    </button> 
                    { !enablePasswordInput ?  
                     <button 
                     className='btn' 
                     type="button" 
                     onClick={async () => {
                      const result = await triggerValidation("userId");
                      if(result)
                      {
                        const userId=getValues('userId');
                        chkUserId(userId);
                      }
                    }}
                     >
                      Check UserID
                     </button>  : 
                
                    <button 
                     className='btn' 
                     type="button"
                     onClick={async () => {
                      const result = await triggerValidation(["userId","password"]);
                      if(result)
                      {
                        const formData=getValues();
                        submitData(formData);
                      }
                    }}
                     >
                     Submit
                    </button>
                }

                  </div>
                </form>
              </div>
            </div>
          </div>
          <div className="rhs">
            <CreateId/>
          </div>
        </div>
      </div>            
      <Loader show={loader} />
    </>
  );
};

export default LoginUserId;
