import React from 'react'

const CreateId = () => {
    return (
        <div className="m-creat-id">
            <div className="decs">
            <h3 className="title">
                Existing customer but don't have an Aditya Birla Capital One
                ID?
            </h3>
            <label className="info info">
                <span className="icon icon-info"></span>
                <span className="text">Create your One ABC ID</span>
            </label>
            </div>
            <div className="act-wrap">
            <button type="button" className="btn btn-primary btn-white"   >
                Create Your ID
            </button>
            </div>
        </div>
    )
}

export default CreateId;
