import React from 'react';


const LoginPopup = props => {
return(
    <>
    <div className={`modal ${(props.ABCidModalState)?'active':''}`}>
        <div className="dialog">
          <div className="content">
            <div className="modal-header">
              <h5 className="title">ABC One ID sent</h5>
              <button type="button" className="btn btn-icon" onClick={props.closeModalHandler}>
                <span className="icon icon-cancel"></span>
              </button>
            </div>
            <div className="modal-body">
              <p className="para">Your ABC One ID has been sent to your registered mobile or email ID</p>
            </div>
            <div className="act-btn">
              <button type="button" className="btn">Go to Login</button>
            </div>
          </div>
        </div>
      </div>
      <span className={`overlay ${(props.ABCidModalState)?'active':''}`}></span>
      
      
  </>

);
}
export default LoginPopup;