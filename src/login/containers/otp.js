import React,{useEffect,useState,useRef } from 'react'
import { NavLink } from 'react-router-dom';
import CreateId from '../containers/createId';


const OTP = props => {
    
    const { setEmptyerror } = props;
    const seconds=60;
    const [timeLeft, setTimeLeft] = useState(seconds);
    const [timererror,settimerError] = useState(false);
    const [textoneBox,setoneBox]=useState('');
    const [texttwoBox,settwoBox]=useState('');
    const [textthreeBox,setthreeBox]=useState('');
    const [textfourBox,setfourBox]=useState('');
    // const [textfiveBox,setfiveBox]=useState('');
    // const [textsixBox,setsixBox]=useState('');
    const [buttonDisable,setbuttonDisable]=useState(true);
    const [ displayOtperror,setdisplayOtperror ]=useState(false);
    const [finalOtpData,setfinalOtpData]=useState('');
    const [resendButton,setresendButton]=useState(true);
    const [disableInput,setdisableInput]=useState(false);

    const otp1=useRef();
    const otp2=useRef();
    const otp3=useRef();
    const otp4=useRef();

    const inlineStyle={
        color:'red'

    }

    const goBack = event => {
        event.preventDefault();
        props.goBackevent();
    
    }
   

    const handleOtp = event  => {
           event.preventDefault();
           if(finalOtpData.length!==4)
           {
            setdisplayOtperror(true);
           }else{
            props.otpData(finalOtpData);
           }
        }

    const handleResend = () => {
        settimerError(false);
        setresendButton(true)
        setbuttonDisable(true);
        setTimeLeft(60);
        setdisableInput(false)
        otp1.current.focus()
        props.reSend();
    }

    const handleValue = event =>{

        let mergeData = textoneBox.toString()+texttwoBox.toString()+textthreeBox.toString()+
                      textfourBox.toString();
                       
      if(mergeData.length===4)
            {
                setbuttonDisable(false);
                setdisplayOtperror(false);
                setfinalOtpData(mergeData);
          } else{
                setbuttonDisable(true);
                setdisplayOtperror(true);
                props.otpErrorStatus(false);
              } 
      
        //Focus Input Logic
         if(event.target.name==='txt1' && textoneBox)
        {
            otp2.current.focus()
        }else if(event.target.name==='txt2' && texttwoBox)
        {
           otp3.current.focus()
        }else if(event.target.name==='txt3' && textthreeBox)
        {
            otp4.current.focus()
        }
       //BackSpace && Delete Logic  
      if(event.keyCode===8 || event.keyCode===46)
        {
            switch(event.target.name)
            {
                case 'txt2':
                   return otp1.current.focus();
                case 'txt3' :
                    return otp2.current.focus();
                 case 'txt4' :
                     return otp3.current.focus();
                 default :
                    return otp1.current.focus()    
           }
      }

         
    
    } 

  
    useEffect(() => {
     // console.log('prasad')  
     // exit early when we reach 0
    if (!timeLeft) {
        setdisplayOtperror(false);
        setoneBox('');
        settwoBox('');
        setthreeBox('');
        setfourBox('');
        setEmptyerror();
        setdisableInput(true);
        settimerError(true);
        setresendButton(false)
        setbuttonDisable(true);
        return;
    }
    
       //otp1.current.focus();
    // save intervalId to clear the interval when the
    // component re-renders
    const intervalId = setInterval(() => {
       setTimeLeft(timeLeft - 1);
    }, 1000);

    // clear interval on re-render to avoid memory leaks
    return () => clearInterval(intervalId);
    
    // add timeLeft as a dependency to re-rerun the effect
    // when we update it
  }, [timeLeft]);
    


    return (
    <>
      <div className="container-fluid">
                <div className="lyt-login">
                    <div className="lhs">
                    <NavLink className="btnBack" to="" onClick={ goBack } title="Go Back">
                    <span className="icon icon-arrow-left"></span>
                    <span className="text">Go Back</span>
                    </NavLink>
                    <div className="m-login-journey">
                    <div className="head">
                        <h1 className="title">Enter OTP</h1>
                        <p className="para">{props.maskedData} </p>
                    </div>
                    <div className="cont card">
                        <form>
                            <div className={timererror ?'input-group otp show-error':'input-group otp'}  >
                                <label className="lbl">Enter OTP</label>
                                <input type="text" className="form-control mobile" placeholder="Enter OTP"/>
                                <div className="otp-box desktop">
                                <input autoFocus type="text" 
                                    className="form-control"
                                    value={textoneBox} 
                                    name="txt1"
                                    disabled={disableInput}
                                    maxLength={1}
                                    ref={otp1}
                                    autoComplete="off"
                                    onKeyUp={ handleValue }
                                    onChange= {event => {
                                        if (isNaN(Number(event.target.value))) {
                                            return;
                                          } else {
                                            setoneBox(event.target.value );
                                           // otp2.current.focus()
                                          }
                                       }} 
                                    />
                                    <input type="text" 
                                    className="form-control"
                                    value={texttwoBox} 
                                    maxLength={1}
                                    ref={otp2}
                                    name="txt2" 
                                    autoComplete="off"
                                    disabled={disableInput} 
                                    onKeyUp={ handleValue }
                                    onChange= {event => {
                                        if (isNaN(Number(event.target.value))) {
                                            return;
                                          } else {
                                            settwoBox(event.target.value );
                                           // otp3.current.focus()
                                          }
                                       }} 
                                    />
                                    <input type="text" 
                                    className="form-control" 
                                    value={textthreeBox} 
                                     maxLength={1}
                                     ref={otp3}
                                     name="txt3"
                                     autoComplete="off"
                                     disabled={disableInput}
                                     onKeyUp={ handleValue }
                                     onChange= {event => {
                                        if (isNaN(Number(event.target.value))) {
                                            return;
                                          } else {
                                            setthreeBox(event.target.value );
                                            //otp4.current.focus()
                                          }
                                       }} 
                                    />
                                    <input type="text" 
                                    className="form-control"
                                    value={textfourBox}
                                    maxLength={1}
                                    name="txt4" 
                                    ref={otp4}
                                    autoComplete="off"
                                    disabled={disableInput}
                                    onKeyUp={ handleValue }
                                    onChange= {event => {
                                        if (isNaN(Number(event.target.value))) {
                                            return;
                                          } else {
                                            setfourBox(event.target.value );
                                          }
                                       }}
                                  />
                                    {/* <input type="text" 
                                    className="form-control" 
                                    value={textfiveBox}
                                    maxLength={1} 
                                    onKeyUp={ handleValue }
                                    onChange= {event => {
                                        if (isNaN(Number(event.target.value))) {
                                            return;
                                          } else {
                                            setfiveBox(event.target.value );
                                          }
                                       }} 
                                    />
                                    <input type="text" 
                                    className="form-control"
                                    value={textsixBox}
                                     maxLength={1} 
                                     onKeyUp={ handleValue }
                                     onChange= {event => {
                                        if (isNaN(Number(event.target.value))) {
                                            return;
                                          } else {
                                            setsixBox(event.target.value );
                                          }
                                       }}
                                    /> */}
                                </div>

                                <div className="input-group-append">
                                    <span className="input-group-text">
                                        <NavLink 
                                        className={resendButton ? "btn btn-link btn-disabled" : "btn btn-link"} 
                                        to="# " 
                                        onClick={handleResend} 
                                        title="Resend OTP"
                                        >Resend OTP
                                        </NavLink>
                                        <span className="count">{ (timeLeft >=10) ? '00:'+timeLeft : '00:0'+timeLeft }</span>
                                    </span>
                                </div>
                                <span className="error">OTP Timer Expired .Please Use Resend Option</span>
                            </div>
                            <div>
                                    { displayOtperror &&  <h1 style={inlineStyle} >Please Enter Four Digit OTP</h1> }
                                    { props.otpError && <h1 style={inlineStyle} >{props.otpError} </h1> }
                            </div>
                            <div className="act-btn">
                                <button className={ buttonDisable ? 'btn btn-disabled' : 'btn' } onClick={ handleOtp } type="button">Verify</button>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
                    <div className="rhs">
                        <CreateId/>
                    </div>
                </div>
            </div>
        
    </>
    )
}

export default OTP;
