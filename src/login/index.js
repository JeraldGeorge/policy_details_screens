import React, { useState } from 'react';
import LoginTab from './components/loginTab';
import LoginUserid from './components/loginUserid';
import { connect } from 'react-redux';
import { authenticate,networkIssue } from '../actions/auth';
import { useHistory } from 'react-router-dom';
import { Redirect } from 'react-router-dom';




const Login = props => {
    const history = useHistory();
    const [loginUserid, setLoginUserid] = useState(true);
    const [abcId, setabcId] = useState('');
    let isAuthenticated=null;

    if(localStorage.getItem('Auth'))
    {
        isAuthenticated=<Redirect to="/dashboard" /> 
    }


    const buildLoginMenu = () => {
        if (loginUserid) {
            //console.log('prasad');
            return <LoginUserid
                //handleUserid={ chkUserId }  
            handleLogin = { handleLogin }
            history = { history }
            {...props}
            />
        } else {
            return <LoginTab
            goBackfn = { handleBack }
            abcId = { abcId }
            {...props}
            />
        }
    }

    const handleBack = () => {

        setLoginUserid(true);
        // setLoginTab(false);

    }

    const handleLogin = data => {
        //console.log(data);
        setabcId(data);
        setLoginUserid(false);
        //setLoginTab(true);
    }



    return ( <> 
    { isAuthenticated }
   
    { buildLoginMenu() }

        </>

    );
}

const mapStateToProps = state => {
    return {
        authenticate: state.home,
     }
}


const mapDispatchToProps = dispatch => {
    return {
        authenticate: (data,history) => dispatch(authenticate(data,history)),
        networkIssue : data =>dispatch(networkIssue(data))

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);