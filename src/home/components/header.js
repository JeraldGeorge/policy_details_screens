import React from "react";

const Header = () => {
  return (
    <header>
      <div className="container-fluid">
        <div className="header">
          <img src="/abg-assets/images/abc-logo.png" title="ABG Logo" alt="Footer Logo" width="217"/>
        </div>
      </div>
      {/* <div className="container-fluid">
        <div className="outerpadding">
          <div className="row">
            <div className="col-xs-12 col-sm-12">
              <div className="top-navigation top-black">
                <ul>
                  <li>
                    <a href="#" title="Corporates">Corporates</a>
                  </li>
                  <li>
                    <a href="#" title="Advisors">Advisors</a>
                  </li>
                  <li>
                    <a href="#" title="Customer Service">Customer Services</a>
                  </li>
                  <li>
                    <a href="#" title="Careers">Careers</a>
                  </li>
                  <li>
                    <a href="#" title="About us">About us</a>
                  </li>
                </ul>
              </div>
              <div className="top-header-wrapper capital-red-bg">
                <div className="logo">
                  <a href="/">
                    <picture className="">
                      <source
                        media="(min-width: 1021px)"
                        srcset="/abg-assets/images/abc-logo.png"
                      />
                      <source
                        media="(min-width: 768px) and (max-width: 1020px)"
                        srcset="/abg-assets/images/abc-logo-m.png, /abg-assets/images/abc-logo-m.png 2x"
                      />
                      <source
                        media="(max-width: 767px)"
                        srcset="/abg-assets/images/abc-logo-s.png, /abg-assets/images/abc-logo-s.png 2x"
                      />
                      <img src="/abg-assets/images/abc-logo.png" alt="ABCL - Blog" />
                    </picture>
                  </a>
                </div>
                <div className="top-menu">
                  <ul>
                    <li className="level1 has-children pink-link">
                      <span title="Protecting">Protecting</span>
                      <span className="icon-arrow icon">
                        <span className="visuallyhidden">arrow</span>
                      </span>
                      <ul>
                        <li className="level2">
                          <a href="#">Life Insurance</a>
                        </li>
                        <li className="level2">
                          <a href="#">Health Insurance</a>
                        </li>
                        <li className="level2">
                          <a href="#">Multiply Wellness</a>
                        </li>
                        <li className="level2">
                          <a href="#">Motor Insurance</a>
                        </li>
                        <li className="level2">
                          <a href="#">Travel Insurance</a>
                        </li>
                      </ul>
                    </li>
                    <li className="level1 has-children green-link">
                      <span title="Investing">Investing</span>
                      <span className="icon-arrow icon">
                        <span className="visuallyhidden">arrow</span>
                      </span>
                      <ul>
                        <li className="level2">
                          <a href="#">Mutual Funds</a>
                        </li>
                        <li className="level2">
                          <a href="#">Wealth Management</a>
                        </li>
                        <li className="level2">
                          <a href="#">Porfolio Management</a>
                        </li>
                        <li className="level2">
                          <a href="#">Pension Funds</a>
                        </li>
                        <li className="level2">
                          <a href="#">Stocks and Securities</a>
                        </li>
                        <li className="level2">
                          <a href="#">Real Estate Investments</a>
                        </li>
                      </ul>
                    </li>
                    <li className="level1 has-children yellow-link">
                      <span title="Financing">Financing</span>
                      <span className="icon-arrow icon">
                        <span className="visuallyhidden">arrow</span>
                      </span>
                      <ul>
                        <li className="level2">
                          <a href="#">Home Finance</a>
                        </li>
                        <li className="level2">
                          <a href="#">Personal Finance</a>
                        </li>
                        <li className="level2">
                          <a href="#">SME Finance</a>
                        </li>
                        <li className="level2">
                          <a href="#">Loan Against Securities</a>
                        </li>
                        <li className="level2">
                          <a href="#">Real Estate Finance</a>
                        </li>
                      </ul>
                    </li>
                    <li className="level1 has-children red-link">
                      <span title="Advising">Advising</span>
                      <span className="icon-arrow icon">
                        <span className="visuallyhidden">arrow</span>
                      </span>
                      <ul>
                        <li className="level2">
                          <a href="#">MoneyForLife Planner</a>
                        </li>
                        <li className="level2">
                          <a href="#">ABC of Money</a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </div>

                <div className="top-right-wrapper">
                  <span className="top-right-item phone-link">
                    <a href="tel:1800-270-7000">
                      <span className="iconText"> 1800-270-7000</span>
                      <span className="icon-phone icon">
                        <span className="visuallyhidden">telephone</span>
                      </span>
                    </a>
                  </span>

                  <span className="top-right-item home-link">
                    <a href="https://abcscstg.com">
                      <span className="iconText">Home</span>
                      <span className="icon-home icon">
                        <span className="visuallyhidden">Home</span>
                      </span>
                    </a>
                  </span>
                  <span className="top-right-item home-link">
                    <a href="#">
                      <span className="iconText">Dashboard</span>
                    </a>
                  </span>
                  <ul className="dashboard-list">
                    <li className="loggedin-item">
                      <a href="#">
                        <span className="divicon cust-log-ico-mob-user">RP</span>
                      </a>
                      <ul className="login-dropdowm" id="myDropdown">
                      <div className="arrow_up"></div>
                        <div className="arrow_up" style="right: 20.5px;"></div>
                        <li className="fst_ele">
                          <a href="https://abcscstg.com/my-dashboard">
                            My Dashboard
                          </a>
                        </li>
                        <li>
                          <a href="https://abcscstg.com/Selector">
                            Link ABC products
                          </a>
                        </li>
                        <li>
                          <a href="https://abcscstg.com/ChangeContactNo">
                            Change mobile no
                          </a>
                        </li>
                        <li>
                          <a href="https://abcscstg.com/ChangeEmail">
                            Change email address
                          </a>
                        </li>
                        <li>
                          <a href="https://abcscstg.com/ChangePassword">
                            Change password
                          </a>
                        </li>
                        <li className="last_child">
                          <a href="javascript:void(0);" onclick="logoutRequest()">Log Out</a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </div>
                <div className="utility-links hide">
                  <a className="icon-home icon" href="https://abcscstg.com">
                    <span className="visuallyhidden">home</span>
                  </a>
                  <a className="icon-phone icon" href="tel:1800-270-7000">
                    <span className="visuallyhidden">telephone</span>
                  </a>
                  <a className="icon loggedin-name" href="#">
                    <span className="dashboard-list">
                      <span className="loggedin-item">
                        <span className="divicon cust-log-ico-mob-user">RP</span>
                      </span>
                    </span>
                  </a>
                </div>
              </div>
              <div className="mobile-menu-wrapper">
                <div
                  className="hamburger hamburger--elastic"
                  tabindex="0"
                  aria-label="Menu"
                  role="button"
                  aria-controls="navigation"
                  aria-expanded="true/false"
                >
                  <div className="hamburger-box">
                    <div className="hamburger-inner">
                      <span className="visuallyhidden">Menu</span>
                    </div>
                  </div>
                </div>
              </div>
              <div id="mobi-navigation">
                <div className="close-menu">
                  <div
                    className="hamburger hamburger--elastic is-active"
                    tabindex="0"
                    aria-label="Menu"
                    role="button"
                    aria-controls="navigation"
                    aria-expanded="true/false"
                  >
                    <div className="hamburger-box">
                      <div className="hamburger-inner">
                        <span className="visuallyhidden">Close</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="menu-header">
                  <div className="logo"></div>
                  <div className="utility-links"></div>
                </div>
                <div className="menu-content-wrapper">
                  <div className="primary-nav__mobileasd">
                    <div className="primary-nav__mobile">
                      <ul className="">
                        <li className="primary-nav__mobile--headline bold">
                          <span>About Us</span>
                          <i className="fa fa-close primary-nav__close"></i>
                        </li>
                        <li
                          data-toggle="collapse"
                          data-target="#collapseTwo"
                          aria-expanded="true"
                          aria-controls="collapseTwo"
                        >
                          <a>
                            Board of Directors
                            <span className="icon-arrow icon"></span>
                          </a>
                          <ul
                            id="collapseTwo"
                            className="collapse primary-nav__collapse"
                            aria-labelledby="headingOne"
                          >
                            <li>
                              <a href="#">ABSLI DigiShield Plan</a>
                            </li>
                            <li>
                              <a href="#">ABSLI UltimaTerm</a>
                            </li>
                            <li>
                              <a href="#">ABSLI Wealth Assure Plus</a>
                            </li>
                            <li>
                              <a href="#">ABSLI Vision Star Plan</a>
                            </li>
                            <li>
                              <a href="#">ABSLI Cancer Shield Plan </a>
                            </li>
                          </ul>
                        </li>
                        <li
                          data-toggle="collapse"
                          data-target="#collapse2"
                          aria-expanded="true"
                          aria-controls="collapseTwo"
                        >
                          <a>
                            Board of Directors
                            <span className="icon-arrow icon"></span>
                          </a>
                          <ul
                            id="collapse2"
                            className="collapse primary-nav__collapse"
                            aria-labelledby="headingOne"
                          >
                            <li>
                              <a href="#">ABSLI DigiShield Plan</a>
                            </li>
                            <li>
                              <a href="#">ABSLI UltimaTerm</a>
                            </li>
                            <li>
                              <a href="#">ABSLI Wealth Assure Plus</a>
                            </li>
                            <li>
                              <a href="#">ABSLI Vision Star Plan</a>
                            </li>
                            <li>
                              <a href="#">ABSLI Cancer Shield Plan </a>
                            </li>
                          </ul>
                        </li>
                      </ul>
                      <div className="primary-nav__mobile primary-nav__mobile--footer">
                        <p>
                          <span>© 2019, Aditya Birla Capital Ltd.</span>
                          <span>All Rights Reserved.</span>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="menu-content">
                    <div className="mobi-nav-wrapper"></div>
                    <div className="mobi-top-navigation">
                      <ul>
                        <li>
                          <a href="#" title="Corporates">
                            Corporates
                          </a>
                        </li>
                        <li>
                          <a href="#" title="Advisors">
                            Advisors
                          </a>
                        </li>
                        <li>
                          <a href="#" title="Customer Service">
                            Customer Services
                          </a>
                        </li>
                        <li>
                          <a href="#" title="Careers">
                            Careers
                          </a>
                        </li>
                        <li>
                          <a href="#" title="About us">
                            About us
                          </a>
                        </li>
                      </ul>
                    </div>
                    <div className="mobi-footer-links">
                      <ul>
                        <li className="has-children">
                          <a href="javascript:;">Others</a>
                          <span className="icon-arrow icon">
                            <span className="visuallyhidden">arrow</span>
                          </span>
                          <ul>
                            <li>
                              <a
                                href="/about-us/our-solutions"
                                target=""
                                title="Our Solutions"
                              >
                                Our Solutions
                              </a>
                            </li>
                            <li>
                              <a
                                href="/investor-relations"
                                target=""
                                title="Investor Relations"
                              >
                                Investor Relations
                              </a>
                            </li>
                            <li>
                              <a
                                href="/press-and-media"
                                target=""
                                title="Press and Media"
                              >
                                Press and Media
                              </a>
                            </li>

                            <li>
                              <a
                                href="/about-us/our-businesses"
                                target=""
                                title="Our Businesses"
                              >
                                Our Businesses
                              </a>
                            </li>
                            <li>
                              <a
                                href="/about-us/financial-achievements"
                                target=""
                                title="Our Achievements"
                              >
                                Our Achievements
                              </a>
                            </li>
                            <li>
                              <a
                                href="/about-us/csr-and-sustainability"
                                target=""
                                title="CSR"
                              >
                                CSR
                              </a>
                            </li>
                            <li>
                              <a
                                href="/Key-Facts-On-Tax-Filing"
                                target=""
                                title="ITR Filing"
                              >
                                ITR Filing
                              </a>
                            </li>

                            <li>
                              <a
                                href="/branch-locator"
                                target=""
                                title="Locate Us"
                              >
                                Locate Us
                              </a>
                            </li>
                          </ul>
                        </li>
                        <li>
                          <a href="#" target="" title="Privacy Policy">
                            Privacy Policy
                          </a>
                        </li>
                        <li>
                          <a href="#" target="" title="Terms and Conditions">
                            Terms and Conditions
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div className="menu-footer brand-capital-red-bg">
                    <p>
                      <span>© 2019, Aditya Birla Capital Ltd.</span>
                      <span>All Rights Reserved.</span>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> */}
    </header>
  );
};

export default Header;
