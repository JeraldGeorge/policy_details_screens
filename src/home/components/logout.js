import React,{ useEffect } from 'react';
import { connect } from 'react-redux';
import { logout } from '../../actions/auth';

const Logout = props => {

  const { logout,history } =props;
    useEffect(()=>{
        logout(history);

    },[]);

return(
        <>
      {null }
        </>
  );
}

const mapDispatchToProps = dispatch => {
    return {
        logout: history => dispatch(logout(history)) 

    };
};

export default connect(null,mapDispatchToProps)(Logout)