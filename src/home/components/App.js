import React,{ useEffect } from 'react';
import { connect } from 'react-redux';
import Layout from './layout';
import { BrowserRouter as Router} from 'react-router-dom';
import ErrorPopup from '../../error' 
import { networkIssue } from '../../actions/auth'


//import Loader from '../containers/loader';

const  App = props => {
    
  let {networkIssue } =props;
  const callInitialState = () => {
    if(props && props.networkIssue )
    {
      props.networkIssue(false);
    }
 }
useEffect(() => {
  callInitialState()

},[networkIssue])



  return (
    <React.Fragment>
      <Router> 
         <Layout />
         <ErrorPopup />
     </Router>
     
      {/* <Loader  /> */}
    </React.Fragment>
    
  );
}

const mapDispatchToProps = dispatch => {
  return {
      networkIssue: (data) => dispatch(networkIssue(data)) 

  };
}
export default connect(null,mapDispatchToProps)(App);
