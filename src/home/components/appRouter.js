import React from 'react';
import { Switch,Route,Redirect} from 'react-router-dom';
import { connect } from 'react-redux';
import Register from '../../register';
import Login from '../../login';
import ForgotPassword from '../../login/components/forgotPassword';
import ForgotUser from '../../login/components/forgotUser';
import Dashboard from '../../dashboard/components'
// import PayPremium from '../../paypremium'
import Logout from './logout';
import PolicyListing from '../../policyDetails/components/PolicyListing';
import PolicyDetail from '../../policyDetails/components/PolicyDetail';
import UpdateNominee from '../../policyDetails/containers/UpdateNominee';
import BankDetails from '../../policyDetails/containers/BankDetails';
import PolicyScreens from '../../policyDetails';
import PayPremiumPolicyListing from '../../paypremium/components/PayPremiumPolicyListing';



function AppRouter()
{
   
const ProtectedRoutes = ({component:Component,...rest }) =>{
 return <Route {...rest} render={(props) =>{
     return localStorage.getItem('Auth') ? <Component {...props} /> : 
     <Redirect to="/" />
 }} />

}    
   
    
return(
        <>
        <Switch>
        <Route path="/" exact component={ Login } />
        <Route path="/register" component ={ Register } />
        <Route path="/forgotPassword"  component={ ForgotPassword } />
        <Route path="/forgotUser" component={ ForgotUser } />
        <ProtectedRoutes path="/dashboard" component={ Dashboard } />
        {/* <ProtectedRoutes path="/paypremium" component={ PayPremium } /> */}
        <ProtectedRoutes path="/logout" component={Logout} />
        <ProtectedRoutes path="/policylisting" component={PolicyListing} />
        <ProtectedRoutes path="/policy-detail" component={PolicyDetail} />
        <ProtectedRoutes path="/updatenominee" component={UpdateNominee} />
        <ProtectedRoutes path="/bank-detail" component={BankDetails} />
        <ProtectedRoutes path="/payprimium-policylist" component={PayPremiumPolicyListing} />
        </Switch>
        </>
    );
}





export default connect()(AppRouter);