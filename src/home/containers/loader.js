import React from 'react'

const Loader = (props) => {
    return (
        <>
            <span className={`bs-loader ${(!props.show)?'hide':''} `}></span>
        </>
    )
}

export default Loader;
