import React from 'react';
import { connect } from 'react-redux';
import { networkIssue } from '../actions/auth'

const ErrorPopup = props => {


const closeModalHandler = () => {
props.networkIssue(false);
}


//console.log(props.network.ErrorPopup)

return(
    <>
    <div className={`modal ${(props.network && props.network.errorPopup)?'active':''}`}>
        <div className="dialog">
          <div className="content">
            <div className="modal-header">
              <h5 className="title">Aditya Birla Capital </h5>
              <button type="button" className="btn btn-icon" onClick={closeModalHandler}>
                <span className="icon icon-cancel"></span>
              </button>
            </div>
            <div className="modal-body">
              <p className="para"> We are Currently facing Network Issue.Try to Login Later  </p>
            </div>
            <div className="act-btn">
              <button type="button" className="btn">Go to Login</button>
            </div>
          </div>
        </div>
      </div>
      <span className={`overlay ${(props.network && props.network.ErrorPopup)?'active':''}`}></span>
      
      
  </>

);
}

const mapStateToProps = state => {
    return {
        network: state.home
    }
}


const mapDispatchToProps = dispatch => {
    return {
        networkIssue: (data) => dispatch(networkIssue(data)) 

    };
};



export default connect(mapStateToProps,mapDispatchToProps)(ErrorPopup)