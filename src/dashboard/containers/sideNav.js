import React,{useState,useEffect} from "react";
import useGetPositions from '../../hooks/useGetPositions';
import { isMobile } from "react-device-detect"; 
import { NavLink,withRouter } from "react-router-dom";

const SideNav = (props) => {
  const [className, setClassName] = useState("");
  const [navState,setNavState]= useState(true);
  const [hvrClass,setHvrClass] = useState('') ;
  const [docHeight,setDocHeight] = useState(0)
  const [sideNavPosition] = useGetPositions('refContainer');
  let body = document.body,
            html = document.documentElement

  const navStateHandler = (e)=>{
    e.preventDefault();
    setNavState(false);
    setTimeout(()=>{
      setHvrClass('hvr')
    },1000)
  }
  useEffect(()=>{
    if(!isMobile){
      setDocHeight(Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ));
    }else{
      setDocHeight(0)
    }
  },[])
  const handleLeftMenuDock = () => {
    const position = window.pageYOffset;
    if (position >= 162) setClassName("fixed");
    else setClassName("");
  };
  useEffect(() => {
    window.addEventListener("scroll", handleLeftMenuDock, { passive: true });

    return () => {
      window.removeEventListener("scroll", handleLeftMenuDock);
    };
  }, []);
  return (
    <>
      <div className={`bs-side-nav ${className}`} style={{'left':`${(className)?sideNavPosition.left+'px':''}`}}>
        <div className={`side-nav ${(navState)?'active':''} ${(props.openStateMobile)?'open':''} ${hvrClass} `} style={{'height':`${(docHeight)>0?docHeight+'px':'auto'}`}}>
          <button className="btn open-close" onClick={(e)=>{navStateHandler(e)}} >
            <span className="close">
              <span className="line-break">View</span>
              <span className="line-break">Menu</span>
            </span>
            <span className="open">
              <span className="text">Menu</span>
              <span className="icon icon-cancel"></span>
            </span>
          </button>
          <div className="nav-list">
            <ul className="list">
              <li className="list-item">
                <NavLink className="link" to="" title="View Profile">
                  <span className="icon user-name">KR</span>
                  <span className="text">View Profile</span>
                </NavLink>
              </li>
              <li className="list-item">
                <NavLink className="link" to="" title="Dashboard">
                  <span className="icon icon-dashboard"></span>
                  <span className="text">Dashboard</span>
                </NavLink>
              </li>
              <li className="list-item">
                <NavLink className="link" to="" title="Policy Details">
                  <span className="icon icon-policy-details"></span>
                  <span className="text">Policy Details</span>
                </NavLink>
              </li>
              <li className="list-item">
                <NavLink className="link" to="" title="Pay premium">
                  <span className="icon icon-pay"></span>
                  <span className="text">Pay premium</span>
                </NavLink>
              </li>
              <li className="list-item">
                <NavLink className="link" to="" title="Downloads">
                  <span className="icon icon-download"></span>
                  <span className="text">Downloads</span>
                </NavLink>
              </li>
              <li className="list-item">
                <NavLink className="link" to="" title="Service Requests">
                  <span className="icon icon-service-request"></span>
                  <span className="text">Service Requests</span>
                </NavLink>
              </li>
              <li className="list-item">
                <NavLink className="link" to="" title="Status tracker">
                  <span className="icon icon-status-tracker"></span>
                  <span className="text">Status tracker</span>
                </NavLink>
              </li>
              <li className="list-item">
                <NavLink className="link" to="" title="FAQs">
                  <span className="icon icon-faq"></span>
                  <span className="text">FAQs</span>
                </NavLink>
              </li>
              <li className="list-item">
                <NavLink className="link" to="" title="Terms and Conditions">
                  <span className="icon icon-tc"></span>
                  <span className="text">Terms and Conditions</span>
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </>
  );
};

export default withRouter(SideNav);
