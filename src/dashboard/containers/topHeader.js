import React from 'react';
import { isMobile } from "react-device-detect";
import { NavLink } from "react-router-dom"; 

const TopHeader =props => {
    
return(
    <>
 
          {/* {start content} */}
          <div className="actionHeader">
            {isMobile ? (
              <h1 className="actionHeader__title" onClick={props.sideBarHandlerMobile()}>
                Dashboard
                <span className="icon icon-chevron-down"></span>
              </h1>
            ) : (
                <h1 className="actionHeader__title">
                  Dashboard
                  <span className="icon icon-chevron-down"></span>
                </h1>
              )}
            <ul className="iconList">
              <li className="iconList__item">
                <NavLink to="" className="icon icon-search" title="search icon"></NavLink>
              </li>
              <li className="iconList__item">
                <NavLink to="" title="what's app icon" onClick={(e) => { props.whatsappModalStateHandler(e, "open"); }} className="icon icon-whats-app"></NavLink>
              </li>
              <li className="iconList__item desktop">
                <NavLink to="" className="icon icon-call" title="call icon"></NavLink>
              </li>
              <li className="iconList__item desktop">
                <NavLink to="" className="icon icon-branch-locator" title="branch locator icon"></NavLink>
              </li>
              <li ref={props.node} className="iconList__item">
                <NavLink to="" title="notification icon" className="icon icon-notification" onClick={(e) => { props.notificationStateHandler(e); }} ></NavLink>
                <div className={`iconList__submenu ${props.notificationInfoState ? "iconList--active" : ""}`}>
                  <div  className="notificationInfo">
                    <button  className="notificationInfo__cancel btn btn--icon" onClick={(e) => { props.notificationStateHandler(e); }}>
                      <span className="icon icon-cancel"></span>
                    </button>
                    <div  className="notificationInfo__cont">
                      <h3 className="notificationInfo__title">
                        Your last transaction
                      </h3>
                      <ul className="notificationInfo__list">
                        <li className="notificationInfo__item">
                          <NavLink to="" title="New address details verified for Term Insurance Policy No. 124346983" >
                            New address details verified for <strong>Term Insurance Policy No. 124346983</strong>
                          </NavLink>
                        </li>
                        <li className="notificationInfo__item">
                          <NavLink to="" title="Request processing. Change should reflect within 04 to 05 working days" >
                            Request processing. Change should reflect within 04 to 05 working days
                          </NavLink>
                        </li>
                        <li className="notificationInfo__item">
                          <NavLink to="" title="Please add Purpose of Insurance for your Term Insurance Policy No. 1234346983">
                            Please add Purpose of Insurance for your <strong> Term Insurance Policy No. 1234346983</strong>. This will help us serve you better in the future.
                          </NavLink>
                          <NavLink to="" title="Add now" className="notificationInfo__paynow btn btn--link">
                            Add now
                          </NavLink>
                        </li>
                        <li className="notificationInfo__item">
                          <NavLink to="" title="New address details verified for Term Insurance Policy No. 124346983">
                            New address details verified for{" "}
                            <strong>Term Insurance Policy No. 124346983</strong>
                          </NavLink>
                        </li>
                        <li className="notificationInfo__item">
                          <NavLink
                            to=""
                            title="Request processing. Change should reflect within 04 to 05 working days"
                          >
                            Request processing. Change should reflect within 04
                            to 05 working days
                          </NavLink>
                        </li>
                        <li className="notificationInfo__item">
                          <NavLink
                            to=""
                            title="Please add Purpose of Insurance for your Term Insurance Policy No. 1234346983"
                          >
                            Please add Purpose of Insurance for your{" "}
                            <strong>
                              Term Insurance Policy No. 1234346983
                            </strong>
                            . This will help us serve you better in the future.
                          </NavLink>
                          <NavLink
                            to=""
                            title="Add now"
                            className="notificationInfo__paynow btn btn--link"
                          >
                            Add now
                          </NavLink>
                        </li>
                        <li className="notificationInfo__item">
                          <NavLink
                            to=""
                            title="New address details verified for Term Insurance Policy No. 124346983"
                          >
                            New address details verified for{" "}
                            <strong>Term Insurance Policy No. 124346983</strong>
                          </NavLink>
                        </li>
                        <li className="notificationInfo__item">
                          <NavLink
                            to=""
                            title="Request processing. Change should reflect within 04 to 05 working days"
                          >
                            Request processing. Change should reflect within 04
                            to 05 working days
                          </NavLink>
                        </li>
                        <li className="notificationInfo__item">
                          <NavLink
                            to=""
                            title="Please add Purpose of Insurance for your Term Insurance Policy No. 1234346983"
                          >
                            Please add Purpose of Insurance for your{" "}
                            <strong>
                              Term Insurance Policy No. 1234346983
                            </strong>
                            . This will help us serve you better in the future.
                          </NavLink>
                          <NavLink
                            to=""
                            title="Add now"
                            className="notificationInfo__paynow btn btn--link"
                          >
                            Add now
                          </NavLink>
                        </li>
                      </ul>
                    </div>
                    <NavLink
                      to=""
                      title="View all"
                      className="notificationInfo__viewall btn btn-primary"
                    >
                      View all
                    </NavLink>
                  </div>
                </div>
              </li>
            </ul>
          </div>
          </>



)


}
export default TopHeader;