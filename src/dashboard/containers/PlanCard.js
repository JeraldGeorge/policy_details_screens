import React,{useEffect,useRef,useState} from "react";
import PlanCardProgress from "./PlanCardProgress";
import Swiper from "swiper";
import KebabUlip from '../containers/kebabUlip';
import Kebab from '../containers/kebab';
import KebabAnnunity from '../containers/kebabAnnunity';



const PlanCard = props => {
  const node = useRef();
  let bsPlanCardSwiper;
  const [kebabMenuState,setKebabMenuState] = useState(false)
  const handleClickOutside = (e)=>{
    if (node.current.contains(e.target)) {
      // inside click
      return;
    }
    setKebabMenuState(false);
    // outside click 
  }
  useEffect(() => {
    //console.log(props.finance)
  bsPlanCardSwiper = new Swiper(".js-planCard-swiper", {
      speed: 400,
      slidesPerView: "auto",
      breakpoints: {
        480: {
          slidesPerView: "auto",
        },
      },
      navigation: {
        nextEl: ".js-planCard-swiper .swiper-button-next",
        prevEl: ".js-planCard-swiper .swiper-button-prev",
      },
    });



    // add when mounted
    document.addEventListener("click", handleClickOutside);
    // return function to be called when unmounted
    return () => {
      document.removeEventListener("click", handleClickOutside);
    };
  }, []);
  const setKebabMenuStateHandler=()=>{
    setKebabMenuState(!kebabMenuState);
  }

  let kebabMenu;
  
  const buildKebab = () => {
    if(props.data.POLICY_TYPE==='ULIP')
    {
      kebabMenu= < KebabUlip />  
    }else if(props.data.POLICY_TYPE==='Npar' || props.data.POLICY_TYPE==='Par' || props.data.POLICY_TYPE==='Term' || props.data.POLICY_TYPE==='Health')
    {
      kebabMenu= <Kebab />
    }else if(props.data.POLICY_TYPE==='Annunity')
    {
      kebabMenu=  <KebabAnnunity />
    }else 
    {
      kebabMenu=  <Kebab />;
    }
    return kebabMenu;
 }
   
   

 return (
    <>
    <div  className="m-plan-card typ-goal">
        <div className="head">
          <ul className="list">
            <li className="item">
              <div className="m-lbl-text">
                <label className="lbl-text">Policy No.</label>
                <span className="desc-text">{ props.data.POLICY_NUMBER }</span>
              </div>
            </li>
            <li className="item">
              <div className="m-lbl-text">
                <label className="lbl-text">Policy Type</label>
                <span className="desc-text">
                { props.data.POLICY_TYPE  }
                </span>
              </div>
            </li>
            <li className="item">
              <div className="m-lbl-text">
                <label className="lbl-text">Plan Type</label>
                <span className="desc-text">
                { props.data.PLAN_TYPE }
                </span>
              </div>
            </li>
          </ul>

          <span className="tag active">Active</span>

          <div  className={`dote-menu ${(kebabMenuState)?'active':''}`}>
            <span ref={node} className="icon icon-kebab-menu" onClick={setKebabMenuStateHandler}></span>
            <ul  className="menu">
              { buildKebab() }
            </ul>
          </div>
        </div>
        <div className="cont">
          <PlanCardProgress progress="80" />

          <ul className="list">
            <li className="item">
              <div className="m-lbl-text">
                <label className="lbl-text">Purpose of Insurance</label>
                <span className="desc-text">{ props.data.INSURANCE_PURPOSE } </span>
              </div>
            </li>
            <li className="item">
              <div className="m-lbl-text">
                <label className="lbl-text">Sum Assured</label>
                <span className="desc-text">&#8377; {  props.data.TOTAL_SUM_ASSURED}</span>
              </div>
            </li>
            <li className="item">
              <div className="m-lbl-text">
                <label className="lbl-text">Total Premium</label>
                <span className="desc-text">&#8377; { props.data.POLICYPREMIUM }</span>
              </div>
            </li>
            <li className="item">
              <div className="m-lbl-text">
                <label className="lbl-text">Fund Value</label>
                 <span className="desc-text">&#8377; 0.0</span> 
              </div>
            </li>
            <li className="item">
              <div className="m-lbl-text">
                <label className="lbl-text">Policy Term</label>
                <span className="desc-text">{ props.data.POLICY_TERM }</span>
              </div>
            </li>
            {/* <li className="item">
              <div className="m-lbl-text">
                <label className="lbl-text">NAV</label>
                <span className="desc-text">&#8377; 13.70</span>
              </div>
            </li> */}
            <li className="item">
              <div className="m-lbl-text">
                <label className="lbl-text">Premium Due</label>
                <span className="desc-text">23 Mar 2020</span>
              </div>
            </li>
          </ul>

          <div className="act-btn">
            <button className="btn btn-outline" type="button">
              Details
            </button>
            <button className="btn" type="button">
              Pay Premium
            </button>
          </div>
        </div>
        {/* <div className="upgrade">
          <h3 className="title">
            <span className="icon icon-star"></span>
            <span>Congratulations!</span>
          </h3>
          <p className="para">
            Special offer for you! Get a cover of Cr for premium of
            &#8377;10,000
          </p>
          <button className="btn btn-link" type="button">
            Upgrade Now
          </button>
        </div> */}
      </div>
    
    </>
  );
};

export default PlanCard;
