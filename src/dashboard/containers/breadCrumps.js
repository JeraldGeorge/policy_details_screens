import React ,{ useEffect } from 'react';
import { NavLink  } from 'react-router-dom';
import Swiper from "swiper";
const BreadCrumps = props => {

  let mIconTextswiper;

 useEffect(()=> {
  mIconTextswiper = new Swiper(".js-icontext-swiper", {
    speed: 400,
    slidesPerView: "auto",
    breakpoints: {
      480: {
        slidesPerView: 6,
      },
    },
  });
},[]); 

return(
  
    <div className="bs-section">
            <div className="sec-head">
              <h3 className="sec-title">Welcome {props.customer}</h3>
            </div>
            <div className="sec-cont">
              <div className="swiper-container js-icontext-swiper">
                <div className="swiper-wrapper">
                  <div className="swiper-slide">
                    <NavLink
                      to="#"
                      className="m-icon-text"
                      title="Upgrade"
                    >
                      <span className="icon icon-upgrade"></span>
                      <span className="text">Upgrade</span>
                    </NavLink>
                  </div>
                  <div className="swiper-slide">
                    <NavLink to="payprimium-policylist" className="m-icon-text" title="Pay Premium">
                      <span className="icon icon-pay"></span>
                      <span className="text">Pay Premium</span>
                    </NavLink>
                  </div>
                  <div className="swiper-slide">
                    <NavLink
                      to="#"
                      className="m-icon-text"
                      title="Download Tax Certificate"
                    >
                      <span className="icon icon-download-certificate"></span>
                      <span className="text">
                        Download Tax Certificate
                      </span>
                    </NavLink>
                  </div>
                  <div className="swiper-slide">
                    <NavLink
                      to="#"
                      className="m-icon-text"
                      title="Set Standing Instructions"
                    >
                      <span className="icon icon-instructions"></span>
                      <span className="text">
                        Set Standing Instructions
                      </span>
                    </NavLink>
                  </div>
                  <div className="swiper-slide">
                    <NavLink to="#" className="m-icon-text" title="Set Alerts">
                      <span className="icon icon-time"></span>
                      <span className="text">Set Alerts</span>
                    </NavLink>
                  </div>
                  <div className="swiper-slide">
                    <NavLink to="#" className="m-icon-text" title="Service Requests">
                      <span className="icon icon-service-request"></span>
                      <span className="text">Service Requests</span>
                    </NavLink>
                  </div>
                </div>
              </div>
            </div>
          </div>
   

);


}

export default BreadCrumps;