import React from 'react';
import QuickActions from "./quickActions";
import { isMobile } from "react-device-detect";

const ActionHeader = props => {

return(
  <div className="bs-action-header">
            {isMobile ? (
              <h1 className="title" onClick={props.sideBarHandlerMobile}>
                Dashboard
                <span className="icon icon-chevron-down"></span>
              </h1>
            ) : (
                <h1 className="title">
                  Dashboard
                  <span className="icon icon-chevron-down"></span>
                </h1>
              )}
            <QuickActions customer={props.customer } />
   </div>
);

}
export default ActionHeader;