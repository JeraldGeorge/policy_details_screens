import React from 'react'

function PlanCardProgress(props) {
    return (
        <div className="m-goal">
            <label className="goal-lbl">Goal achieved</label>
            <span className="status-bar">
                <span className="total"></span>
                <span className="achive" style={{'width':props.progress+'%'}}></span>
            </span>
            <span className="status">
                <span>{props.progress}</span>
                <span>%</span>
            </span>
        </div>
    )
}

export default PlanCardProgress
