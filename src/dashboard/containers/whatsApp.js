import React from 'react';
import BannerAdvertise from './BannerAdvertise';

const WhatsApp = props => {

return(
<>
<div className="bs-section">
            <div className="sec-cont">
              <BannerAdvertise/>
              <div className="m-whtsp-acc">
                <div className="lhs">
                  <h3 className="title">
                    Manage your account on WhatsApp
                  </h3>
                </div>
                <div className="rhs">
                  <h4 className="subtitle">
                    Mobile Number
                  </h4>
                  <div className="whtsp-ctrl form-group">
                    <span className="icon icon-whats-app"></span>
                    <input
                      type="text"
                      className="whtsp-input form-control" value={ props.mobile } readOnly
                    />
                    <button className="btn btn-icon">
                      <span className="icon icon-arrow-right"></span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
</>

);


}

export default WhatsApp;