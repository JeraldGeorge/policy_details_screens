import React, { useEffect, useState } from "react";
import { connect } from 'react-redux';
import { isMobile } from "react-device-detect";
import SideNav from "../../home/containers/sideNav";
import PlanCard from "../containers/PlanCard";
import FloatingMenu from "../containers/floatingmenu";
import Loader from "../../home/containers/loader";
import useSetBgImage from "../../hooks/useSetBgImage";
import { NavLink } from "react-router-dom";
import ActionHeader from '../containers/actionHeader';
import BreadCrumps from "../containers/breadCrumps";
import WhatsApp from "../containers/whatsApp";
import { getDashboardDetails } from '../../actions/dashboard';



const  Dashboard = props =>{
  const [loader,setLoader] = useState(true);
  const [covidState, setCovidState] = useState(true);
  const [sidebarMobileState, setSidebarMobileState] = useState(false);
  const [playVideoState, setPlayVideoState] = useState(false);
  
 
 const { getDashboardDetails } = props;
 

  useEffect(() => {
    getDashboardDetails(); 
    setLoader(false);
   
 
  },[getDashboardDetails]);
  
  const covidCloseHandler = () => {
    setCovidState(false);
  };
  
  const sideBarHandlerMobile = () => {
    if (sidebarMobileState) {
      setSidebarMobileState(false);
    } else {
      setSidebarMobileState(true);
    }
  };
  const openVideoModalHandler = (e, param) => {
    e.preventDefault();
    if (param === "open") {
      setPlayVideoState(true);
    } else {
      setPlayVideoState(false);
    }
  };
  
  useSetBgImage('.setBgSrc', '.getBgSrc')
  return (
    <>
     <div className="container-fluid refContainer">
        <SideNav  
        openStateMobile={sidebarMobileState} 
        customer={(props && props.userInfo) ?  props.userInfo : 'Guest'}
        />
        <div className="content-wrapper">
          {/* {start content} */}
         <ActionHeader 
         sideBarHandlerMobile={sideBarHandlerMobile} 
         customer={ props.userInfo ? props.userInfo : 'Guest'  } 
         />
          <ol className="m-breadcrumb desktop">
            <li className="item">
              <NavLink to="" className="link" title="Home">
                Home<span className="icon icon-chevron-right"></span>
              </NavLink>
            </li>
            <li className="item">Dashboard</li>
          </ol>
         <BreadCrumps 
         customer={ props.userInfo ? props.userInfo.CustomerName : 'Guest'  } 
         />
          {covidState ? (
            <div className="m-covid">
              <div className="desc">
                <h3 className="title">COVID 19</h3>
                <h4 className="subtitle">
                  Our branches are currently closed as per government
                  guidelines. We regret the inconvenience.
                </h4>
                <div className="act-btn">
                  <NavLink to="#" className=" btn btn-link" title="Email us">
                    Email us
                  </NavLink>
                  <NavLink
                    to="#"
                    className=" btn btn-link"
                    title="Connect on Whatsapp"
                  >
                    Connect on Whatsapp
                  </NavLink>
                </div>
              </div>
              <span className="icon icon-info"></span>
              <button
                onClick={covidCloseHandler}
                className="btn-cancel btn btn-icon"
              >
                <span className="icon icon-cancel"></span>
              </button>
            </div>
          ) : null}
          <div className="bs-section">
            <div className="sec-head">
              <h3 className="sec-title">
                Your plan for a secure future is in great shape!
              </h3>
              <h4 className="sec-subtitle">
                Here’s what your portfolio looks like
              </h4>
            </div>
            <div className="sec-cont">
              <div className="swiper-container js-planCard-swiper">
                <div className="swiper-wrapper">
                  {/* <div className="swiper-slide"> */}
                    { props && props.dashBoard &&
                    props.dashBoard.DASHBOARDdDETAILS.map((policy,index) =>{
                   return  <div  className="swiper-slide" key={policy.POLICY_NUMBER }>
                     <PlanCard 
                          data ={ policy }
                          finance={ props.dashBoard.FUNDDETAILS[index]}
                          />
                      </div>
                  })
                  } 
                    {/* <PlanCard />
                  
                  </div> */}
              </div>
                <div className="swiper-button-prev">
                  <span className="icon icon-chevron-left"></span>
                </div>
                <div className="swiper-button-next">
                  <span className="icon icon-chevron-right"></span>
                </div>
              </div>
            </div>
          </div>
          {!isMobile ? (
            <div className="bs-section">
              <div className="sec-cont">
                <div className="m-video setBgSrc">
                  <span className="poster">
                    <img className="getBgSrc" src="/assets/images/video-poster.jpg" title="video poster" alt="video poster" />
                  </span>
                  <button
                    onClick={(e) => {
                      openVideoModalHandler(e, "open");
                    }}
                    className="icon icon-video"
                  ></button>
                </div>
              </div>
            </div>
          ) : null}
         {/* <WhatsApp 
          mobile={props.userInfo ? props.userInfo.MobileNo : '' }
         /> */}
          {isMobile ? (
            <div className="bs-section">
            <div className="sec-cont">
              <div className="m-video setBgSrc">
                <span className="poster">
                  <img className="getBgSrc" src="/assets/images/video-poster.jpg" title="video poster" alt="video poster" />
                </span>
                <button
                  onClick={(e) => {
                    openVideoModalHandler(e, "open");
                  }}
                  className="icon icon-video"
                ></button>
              </div>
            </div>
          </div>
          ) : null}
          <div className="bs-section">
            <div className="sec-head">
              <h3 className="sec-title">Upcoming Activity</h3>
            </div>
            <div className="sec-cont">
              <dl className="m-activity-list">
                <dt className="title">ABSLI Health Plan</dt>
                <dd className="desc">
                  <span className="desctext">
                    Premium due - 1 April 2020
                  </span>
                  <NavLink to="" className="btn btn-link" title="Pay now">
                    Pay now
                  </NavLink>
                </dd>
                <dt className="title">ABSLI Annuity Plan</dt>
                <dd className="desc">
                  <span className="desctext">
                    Initiated claim - 6 April 2020
                  </span>
                  <NavLink to="" className="btn btn-link" title="Track claim">
                    Track claim
                  </NavLink>
                </dd>
                <dt className="title">ABSLI Par Plan</dt>
                <dd className="desc">
                  <span className="desctext">
                    Loan approved - 12 February 2020
                  </span>
                  <NavLink
                    to=""
                    className="btn btn-link"
                    title="Track loan application"
                  >
                    Track loan application
                  </NavLink>
                </dd>
              </dl>
            </div>
          </div>
          {/* {end content} */}
        </div>
      </div>
     
      {playVideoState ? (
        <>
          <div className={`modal ${playVideoState ? "active" : ""}`}>
            <div className="dialog typ-md typ-video">
              <div className="content">
                <div className="modal-header">
                  <button
                    type="button"
                    className="btn btn-icon"
                    onClick={(e) => {
                      openVideoModalHandler(e, "");
                    }}
                  >
                    <span className="icon icon-cancel"></span>
                  </button>
                </div>
                <div className="modal-body">
                  <iframe
                    title="fullFreame"
                    width="100%"
                    height="478"
                    src="https://www.youtube.com/embed/i3vhcvSuXmQ"
                    frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture; fullscreen;"
                    ></iframe>
                </div>
              </div>
            </div>
          </div>
          <span className={`overlay ${playVideoState ? "active" : ""}`}></span>
        </>
      ) : null}
      <FloatingMenu />
      <Loader show={loader} />
      
    </>
  );
}

const mapStateToProps = state => {
  return {
      userInfo: state.home.users,
      dashBoard : state.home.dashboard
  }
}


const mapDispatchToProps = dispatch => {
  return {
      getDashboardDetails: data => dispatch(getDashboardDetails(data)) 

  };
};


export default connect(mapStateToProps,mapDispatchToProps)(Dashboard);
