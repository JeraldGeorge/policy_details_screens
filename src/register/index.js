import React,{useState,useEffect} from "react";
import DateFnsUtils from "@date-io/date-fns";
import { connect } from 'react-redux';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from "@material-ui/pickers";
import { NavLink } from "react-router-dom";
import Loader from "../home/containers/loader";
import CreateId from '../login/containers/createId';
import OTP from '../login/containers/otp';
import { useForm } from 'react-hook-form';
import { registerDetails,verifyOtp } from '../services'
import { networkIssue } from '../actions/auth'
import moment from 'moment';
import { convertFormdata } from '../utils/helper-function';


const  Register = props =>{
  const [loader,setLoader] = useState(true);
  const [selectedDate, setSelectedDate] = useState(null);
  const { register,errors,handleSubmit } = useForm({
    mode: "onBlur"
  });
  const [errorPolicydate,seterrorPolicydate]=useState(false);
  //Otp Handler
  const [otpTab,setotpTab] = useState(false);
  const [otpData,setotpData]=useState('');
  const [otpError,setotpError]=useState('');
  const [otpReference,setotpReference]=useState('');
  const [submitPolicy,setsubmitPoilcy]=useState('');
  const [invalidMessage,setInvalidmessage]=useState('');

  useEffect(()=>{
    setLoader(false);
  },[])

  const handleDateChange = (date) => {
    setSelectedDate(date);
  };

  const onSubmit = async(data) => {
    if(selectedDate===null)
    {
      seterrorPolicydate(true);
      return;  
    }
    setLoader(true)
    const formatDate=moment(selectedDate).format("DD/MM/YYYY");
    const postData = {"api" :"loginWithPassword","policyNumber" : data.policyNumber,"dob": formatDate  }
    const serviceReq= await registerDetails(postData);
    console.log(serviceReq)
    if(serviceReq.data)
    {
     setLoader(false)
     setotpReference(serviceReq.data.insertedId);
     setsubmitPoilcy(postData);
     setotpData(serviceReq.message);
     setotpTab(true);

    }else if(serviceReq.message){
      setLoader(false)
      setInvalidmessage(serviceReq.message);
    }else if(serviceReq.error)
    {
      setLoader(false);
      props.networkIssue(true);
    }else{
      setLoader(false);
    }

  }

  //OTP Handler 

  const chkotpHandler = async(otpData)  => {
  
    //console.log(otpData);
    setLoader(true);
    const postData={"api":"verifyOTP","functionality":"OTP_USER_REGISTER","otpReferenceId":otpReference,"otp":otpData };
    const responseData = await verifyOtp(postData);
    if(responseData.status===10 && responseData.data)
    {
      //setLoader(false);
      const postData = convertFormdata(responseData.data);
      //console.log(postData)
      fetch(responseData.data.redirectUrl, { 
        mode: "no-cors",
        method: 'post', 
        headers: new Headers({
          'Content-Type': 'application/x-www-form-urlencoded',
          'Access-Control-Allow-Origin':'*'
        }), 
        body:postData,
        //processBody:false
      }).catch(err => {
        console.log(err);
    });
  }else if(responseData.status===20){
      setotpError(responseData.message);
      setLoader(false); 
       
    } else if(responseData.error){
      setLoader(false); 
      props.networkIssue(true);
  }else{
   setLoader(false); 
  }
}

const setOtpErrorStatus = data =>{
  setotpError(data);
}

const eventgoBack = () => {
  setotpTab(false);
}


const setEmptyerror = () => {
     setotpError('');
}
const reSend = async() => {
  setLoader(true)
  const data = submitPolicy;
  const serviceReq= await registerDetails(data);
  if(serviceReq && serviceReq.data)
  {
   setLoader(false) 
   setotpReference(serviceReq.data.insertedId);
   setsubmitPoilcy(data);
   setotpData(serviceReq.message);
   setotpTab(true);

  }else if(serviceReq && serviceReq.message){
    setLoader(false)
    setotpError(serviceReq.message);
  } else if(serviceReq && serviceReq.error)
  {
    props.networkIssue();
  }

}





  return (
    <>
     { !otpTab ?
     <div className="container-fluid">
     <div className="lyt-login">
       <div className="lhs">
         <NavLink className="btnBack" to="/" title="Go Back">
           <span className="icon icon-arrow-left"></span>
           <span className="text">Go Back</span>
         </NavLink>
         <div className="m-login-journey">
           <div className="head">
             <h1 className="title">Register</h1>
             <label className="lbl">
               Enter your information below to continue
             </label>
           </div>
           <div className="cont card">
             <form onSubmit={  handleSubmit(onSubmit) }> 
               <div className={invalidMessage || errors.policyNumber ? "form-group show-error" : "form-group"}>
                 <label className="lbl">Policy No./Application No.</label>
                 <input
                   type="text"
                   className="form-control"
                   placeholder="Policy No./Application No."
                   autoComplete="off"
                   name= "policyNumber"
                   ref={register({
                    required: "Please Enter Policy No./Application No",
                    maxLength:10,
                    pattern: {
                      value: /^[a-zA-Z0-9]{3}[0-9]{7}$/,
                      message: "Invalid Policy No./Application No"
                    }
                  })}
                 />
                 <span className="error">
                 { invalidMessage &&  invalidMessage  }
                 {(errors.policyNumber && errors.policyNumber.message) && errors.policyNumber.message}
                 {(errors.policyNumber && errors.policyNumber.type==="maxLength") && 'Maximum Length Exceeded'}
                 </span>
               </div>
               <div className={ errorPolicydate ? "form-group show-error" : "form-group"}>
                 <MuiPickersUtilsProvider utils={DateFnsUtils}>
                   <KeyboardDatePicker
                     autoOk
                     disableFuture
                     inputVariant="standard"
                     openTo="year"
                     format="dd/MM/yyyy"
                     InputLabelProps={{
                       shrink: true,
                     }}
                     label="Policy Owner Date of Birth (DD/MM/YYYY)"
                     placeholder="DD/MM/YYYY"
                     views={["year", "month", "date"]}
                     value={selectedDate}
                     onChange={handleDateChange}
                     keyboardIcon={''}
                     helperText={''}
                   />
                 </MuiPickersUtilsProvider>
                 <span className="error">
                   Please check the Policy No./Application No. and try again
                 </span>
               </div>
               <div className="act-btn">
                 <div className="act-btn-link">
                   <label className="lbl">Already registered?</label>
                   <NavLink className="btn btn-link" to="/" title="Login Now">
                     Login Now
                   </NavLink>
                 </div>
                 <button className="btn" type="submit">
                 Send OTP
                 </button>
               </div>
             </form>
           </div>
         </div>
       </div>
       <div className="rhs">
         <CreateId/>
       </div>
     </div>
   </div>
  : 
  <OTP 
  maskedData={ otpData }  
  otpData={ chkotpHandler } 
  otpError={ otpError } 
  otpErrorStatus={ setOtpErrorStatus }
  goBackevent={ eventgoBack }
  setEmptyerror={ setEmptyerror }
  reSend={ reSend }
  />
  
  }  
      <Loader show={loader} />
    </>
  );
}

const mapDispatchToProps = dispatch => {
  return {
      networkIssue : data =>dispatch(networkIssue(data))
   };
};

export default connect(null,mapDispatchToProps)(Register);
