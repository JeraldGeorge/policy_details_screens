import React, { useEffect, useState } from "react";
import { isMobile } from "react-device-detect";
import Header from "../components/Header";
import Footer from "../components/Footer";
import SideNav from "../components/SideNav";
import FloatingMenu from "../components/FloatingMenu";
import Loader from "../components/Loader";
import { NavLink } from "react-router-dom";
import QuickActions from "../components/QuickActions";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  DatePicker,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import PastPaymentsTable from "../components/PastPaymentsTable";

function PolicyDetailsPastPayments() {
  const [loader, setLoader] = useState(true);
  const [sidebarMobileState, setSidebarMobileState] = useState(false);
  const [selectedDate, setSelectedDate] = useState(null);
  const [selectedDate2, setSelectedDate2] = useState(null);

  useEffect(() => {
    setLoader(false);
  }, []);

  const handleDateChange = (date) => {
    setSelectedDate(date);
  };
  const handleDateChange2 = (date) => {
    setSelectedDate2(date);
  };

  const sideBarHandlerMobile = () => {
    if (sidebarMobileState) {
      setSidebarMobileState(false);
    } else {
      setSidebarMobileState(true);
    }
  };

  return (
    <>
      <Header />
      <div className="container-fluid refContainer">
        <SideNav openStateMobile={sidebarMobileState} />
        <div className="content-wrapper">
          {/* action header start */}
          <div className="bs-action-header">
            {isMobile ? (
              <h1
                className="title"
                onClick={sideBarHandlerMobile}
              >
                Past Payment Details
                <span className="icon icon-chevron-down"></span>
              </h1>
            ) : (
              <h1 className="title">
                Past Payment Details
                <span className="icon icon-chevron-down"></span>
              </h1>
            )}
            <QuickActions />
          </div>
          {/* action header end */}
          {/* breadcrumb starts */}
          <ol className="m-breadcrumb">
            <li className="item">
              <NavLink to="" className="breadcrumb__link" title="Home">
                Home<span className="icon icon-chevron-right"></span>
              </NavLink>
            </li>
            <li className="item">
              <NavLink
                to=""
                className="breadcrumb__link"
                title="Policies and Details"
              >
                Policies and Details
                <span className="icon icon-chevron-right"></span>
              </NavLink>
            </li>
            <li className="item">
              <NavLink
                to=""
                className="breadcrumb__link"
                title="Policies and Details"
              >
                ABSLI Digishield Plan
                <span className="icon icon-chevron-right"></span>
              </NavLink>
            </li>
            <li className="item">Past Payment Details</li>
          </ol>
          {/* breadcrumb end */}
          {/* page template start here */}
          <div className="lyt-past-payments">
            <form>
              <div className="row">
                <div className="col-md-3 col-xs-5">
                  <div className="form-group">
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        autoOk
                        disableFuture
                        inputVariant="standard"
                        openTo="year"
                        format="dd/MM/yyyy"
                        InputLabelProps={{
                          shrink: true,
                        }}
                        label="Start Date"
                        placeholder="DD/MM/YYYY"
                        views={["year", "month", "date"]}
                        value={selectedDate2}
                        onChange={handleDateChange2}
                        keyboardIcon={""}
                        helperText={""}
                      />
                    </MuiPickersUtilsProvider>
                    <span className="error">
                      Please check the Policy No./Application No. and try again
                    </span>
                  </div>
                </div>
                <div className="col-md-3 col-xs-5">
                  <div className="form-group">
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        autoOk
                        disableFuture
                        inputVariant="standard"
                        openTo="year"
                        format="dd/MM/yyyy"
                        InputLabelProps={{
                          shrink: true,
                        }}
                        label="End Date"
                        placeholder="DD/MM/YYYY"
                        views={["year", "month", "date"]}
                        value={selectedDate}
                        onChange={handleDateChange}
                        keyboardIcon={""}
                        helperText={""}
                      />
                    </MuiPickersUtilsProvider>
                    <span className="error">
                      Please check the Policy No./Application No. and try again
                    </span>
                  </div>
                </div>
                <div className="col-md-1 col-xs-1">
                  <button
                    type="button"
                    class="btn btn-icon reset btn-disabled"
                  >
                    <span class="icon icon-cancel"></span>
                  </button>
                </div>
              </div>
            </form>
            <PastPaymentsTable />
          </div>
          {/* page template end here */}
        </div>
      </div>
      <Footer />

      <FloatingMenu />
      <Loader show={loader} />
    </>
  );
}

export default PolicyDetailsPastPayments;
