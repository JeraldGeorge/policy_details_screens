import React, { useEffect, useState } from "react";
import Swiper from "swiper";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import useGetPositions from '../../hooks/useGetPositions';
import { isMobile } from "react-device-detect";
import { NavLink } from "react-router-dom";

// import Header from "../components/Header";
// import Footer from "../components/Footer";
import SideNav from "../../home/containers/sideNav";
import FloatingMenu from "../containers/FloatingMenu";
import Loader from "../../home/containers/loader";
import QuickActions from "../containers/QuickActions";
import PastPaymentsTable1 from "../containers/PastPaymentsTable1";
import { getPolicyDetails } from "../../actions/policydetails";
import { connect } from 'react-redux';
import {policyDetailsObject} from '../containers/policyDetailObject';

function PolicyDetail(props) {
  const [loader, setLoader] = useState(true);
  const [sidebarMobileState, setSidebarMobileState] = useState(false);
  const [ABCidModalState,setABCidModalState] = useState(false);
  const [scrollBtnState,setScrollBtnState] = useState(false)
  const [scrollBtnPosition] = useGetPositions('refContainer');
  let mIconTextswiper;

// console.log('policyDetailsObject',policyDetailsObject)

  useEffect(() => {
    // console.log('test',props.history.location.state)
    setLoader(false);
  }, []);

  // let data = props.history.location.state;
  // console.log('outside data',data)
  const { getPolicyDetails } = props;

  useEffect(() => {
    getdetails();
  },[getPolicyDetails])

  const getdetails = async () => {
    localStorage.setItem('test',JSON.stringify(props.history.location.state))
    getPolicyDetails(props.history.location.state);
    // console.log('checkUser',checkUser)
  }

  const sideBarHandlerMobile = () => {
    if (sidebarMobileState) {
      setSidebarMobileState(false);
    } else {
      setSidebarMobileState(true);
    }
  };

  const closeModalHandler = ()=>{
      setABCidModalState(false);
  }

  mIconTextswiper = new Swiper(".js-icontext-swiper", {
    speed: 400,
    slidesPerView: "auto",
    breakpoints: {
      480: {
        slidesPerView: 6,
      },
    },
  });

  const preventExpanding = (e)=>{
    e.stopPropagation();
  }
  const scrollTopHandler = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    });
  }
  const handleScrollBtnShow = () => {
    const position = window.pageYOffset;
    if (position >= 350) setScrollBtnState(true);
    else setScrollBtnState(false);
  };
  useEffect(() => {
    window.addEventListener("scroll", handleScrollBtnShow, { passive: true });

    return () => {
      window.removeEventListener("scroll", handleScrollBtnShow);
    };
  }, []);

  // const goBack = (e) => {
  //   e.preventDefault();
  //   console.log('goback in detail')
  //   props.handleBack();
  // }

let currentDate = new Date();
let paymentDueDate = new Date("2020/07/01");
let diff = Math.abs(currentDate-paymentDueDate);
let toDays = (diff / (60*60*24*1000))
let days = parseInt(toDays);

  return (
    <>
      {/* <Header /> */}
      <div className="container-fluid refContainer">
        <SideNav openStateMobile={sidebarMobileState} />
        <div className="content-wrapper">
          <div className="lytPolicyDetail">
          {/* action header start */}
          <div className="bs-action-header">
            {isMobile ? (
              <h1
                className="title"
                onClick={sideBarHandlerMobile}
              >
                ABSLI Digishield Plan
                <span className="icon icon-chevron-down"></span>
              </h1>
            ) : (
              <h1 className="title">
                ABSLI Digishield Plan
                <span className="icon icon-chevron-down"></span>
              </h1>
            )}
            <QuickActions whatsappStatus={policyDetailsObject.PolicyDetails.WHATSAPP_IND}/>
          </div>
          {/* action header end */}
          {/* breadcrumb starts */}
          <ol className="m-breadcrumb desktop">
            <li className="item">
              <NavLink to="" className="link" title="Home">
                Home<span className="icon icon-chevron-right"></span>
              </NavLink>
            </li>
            <li className="item">
              <NavLink to="policylisting" className="link" title="Home">
              Policies and Details<span className="icon icon-chevron-right"></span>
              </NavLink>
            </li>
            <li className="item">ABSLI Digishield Plan</li>
          </ol>
          {/* breadcrumb end */}
          {/* page template start here */}

          <div className="bs-section">
            <div className="sec__cont">
              <div className="swiper-container js-icontext-swiper">
                <div className="swiper-wrapper">
                  {days <= 30 && 
                    <div className="swiper-slide">
                      <NavLink to="payprimium-policylist" className="m-icon-text" title="Pay Premium">
                        <span className="icon icon-pay"></span>
                        <span className="text">Pay Premium</span>
                      </NavLink>
                    </div>
                  }
                  <div className="swiper-slide">
                    <NavLink
                      to=""
                      className="m-icon-text"
                      title="Download Tax Certificate"
                    >
                      <span className="icon icon-download-certificate"></span>
                      <span className="text">
                        Download Tax Certificate
                      </span>
                    </NavLink>
                  </div>
                  <div className="swiper-slide">
                    <NavLink
                      to=""
                      className="m-icon-text"
                      title="Set Standing Instructions"
                    >
                      <span className="icon icon-autopay"></span>
                      <span className="text">
                        Set Auto Pay
                      </span>
                    </NavLink>
                  </div>
                  <div className="swiper-slide">
                    <NavLink to="" className="m-icon-text" title="Set Alerts">
                      <span className="icon icon-time"></span>
                      <span className="text">Set Alerts</span>
                    </NavLink>
                  </div>
                  <div className="swiper-slide">
                    <NavLink
                      to=""
                      className="m-icon-text"
                      title="Service Requests"
                    >
                      <span className="icon icon-updateprofile"></span>
                      <span className="text">Update Profile</span>
                    </NavLink>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="bs-section">
            <div className="sec__cont">
              <div className="lyt-accordion">
                
                  <ExpansionPanel square className="mat-accordion">
                    <ExpansionPanelSummary>
                      <div className="accor-head">
                        <h3 className="accor-title">Plan Details</h3>
                        <span className="icon icon-chevron-down"></span>
                      </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                      <div className="accor-body">
                        <div className="m-detail-list">
                          <ul className="row">
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Policy Owner Name
                                </label>
                                <span className="desc-text">
                                  {policyDetailsObject.PolicyDetails.POLICY_OWNER_NAME}
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">Client ID</label>
                                <span className="desc-text">{policyDetailsObject.PolicyDetails.CLIENT_ID}</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Policy Number
                                </label>
                                <span className="desc-text">{policyDetailsObject.PolicyDetails.POL_ID}</span>
                                <span className="m-status-tag active">Active</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">Plan name</label>
                                <span className="desc-text">
                                  ABSLI Digishield
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Purpose of Insurance
                                </label>
                                <span className="desc-text">
                                {policyDetailsObject.PolicyDetails.INSURANCE_PURPOSE}
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  e-Insurance number
                                </label>
                                <span className="desc-text">
                                  {7162415681527}
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Policy Issuance Date
                                </label>
                                <span className="desc-text">{policyDetailsObject.PolicyDetails.ISSUE_DATE}</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Premium Payment Term
                                </label>
                                <span className="desc-text">{policyDetailsObject.PolicyDetails.PAYTERM}</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Policy Term
                                </label>
                                <span className="desc-text">{policyDetailsObject.PolicyDetails.POLICY_TERM}</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Maturity Date
                                </label>
                                <span className="desc-text">{policyDetailsObject.PolicyDetails.MATURITYDATE}</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Contract Expiry Date
                                </label>
                                <span className="desc-text">23/04/2023</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">Payor name</label>
                                <span className="desc-text">Rupa Roy</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Insured name
                                </label>
                                <span className="desc-text">{policyDetailsObject.PolicyDetails.INSURED_NAME}</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Joint Life Name
                                </label>
                                <span className="desc-text">Jane Doe</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Policy Owner Relation with Insured
                                </label>
                                <span className="desc-text">{policyDetailsObject.PolicyDetails.POLICY_OWNER_RELATIONSHIP_WITH_INSURED}</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Base sum assured
                                </label>
                                <span className="desc-text">
                                  &#8377; {policyDetailsObject.PolicyDetails.BASE_SUM_ASSURED}
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Rider sum assured
                                </label>
                                <span className="desc-text">
                                  &#8377; {policyDetailsObject.PolicyDetails.RIDER_SUM_ASSURED}
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Joint Life Sum Assured
                                </label>
                                <span className="desc-text">
                                  &#8377; 30,000
                                </span>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                
                
                  <ExpansionPanel square className="mat-accordion">
                    <ExpansionPanelSummary>
                      <div className="accor-head">
                        <h3 className="accor-title">Rider Details</h3>
                        <span className="icon icon-chevron-down"></span>
                      </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                      <div className="accor-body">
                        <div className="m-detail-list">
                          <div className="detail-head">
                            <h4 className="detail-title">Rider 01</h4>
                            <span className="m-status-tag  active">
                              Active
                            </span>
                          </div>
                          <ul className="row">
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">Name</label>
                                <span className="desc-text">
                                  {policyDetailsObject.RiderDetails[0].RIDER_NAME}
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Sum assured
                                </label>
                                <span className="desc-text">
                                  &#8377; {policyDetailsObject.RiderDetails[0].RIDER_SUM_INSURED}
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                Premium
                                </label>
                                <span className="desc-text">
                                  &#8377; {policyDetailsObject.RiderDetails[0].RIDER_PREMIUM}
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Premium Term
                                </label>
                                <span className="desc-text">{policyDetailsObject.RiderDetails[0].RIDER_TERM}</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Benefit Term
                                </label>
                                <span className="desc-text">{policyDetailsObject.RiderDetails[0].PAY_TERM}</span>
                              </div>
                            </li>
                          </ul>
                        </div>
                        <div className="m-detail-list">
                          <div className="detail-head">
                            <h4 className="detail-title">Rider 02 </h4>
                            <span className="m-status-tag active">Active</span>
                          </div>
                          <ul className="row">
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">Name</label>
                                <span className="desc-text">
                                {policyDetailsObject.RiderDetails[1].RIDER_NAME}
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Sum assured
                                </label>
                                <span className="desc-text">
                                  &#8377; {policyDetailsObject.RiderDetails[1].RIDER_SUM_INSURED}
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                Premium
                                </label>
                                <span className="desc-text">
                                  &#8377; {policyDetailsObject.RiderDetails[1].RIDER_PREMIUM}
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Premium Payment Term
                                </label>
                                <span className="desc-text">{policyDetailsObject.RiderDetails[1].PAY_TERM}</span>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                
                
                  <ExpansionPanel square className="mat-accordion">
                    <ExpansionPanelSummary>
                      <div className="accor-head">
                        <h3 className="accor-title">Premium Details</h3>
                        <div className="accor-act">
                          <button className="btn btn-iconText" onClick={preventExpanding}>
                            <span className="icon icon-edit"></span>
                            <span className="text">Edit</span>
                          </button>
                        </div>
                        <span className="icon icon-chevron-down"></span>
                      </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                      <div className="accor-body">
                        <div className="m-detail-list">
                          <ul className="row">
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Gross Premium
                                </label>
                                <span className="desc-text">
                                  &#8377; {policyDetailsObject.PolicyDetails.GROSS_PREMIUM}
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Modal Premium
                                </label>
                                <span className="desc-text">
                                  &#8377; {policyDetailsObject.PolicyDetails.MODAL_PREMIUM}
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Annual Premium
                                </label>
                                <span className="desc-text">
                                  &#8377; {policyDetailsObject.PolicyDetails.ANNUAL_PREMIUM}
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Next premium due date
                                </label>
                                <span className="desc-text">{policyDetailsObject.PolicyDetails.NEXT_PREMIUM_DUE}</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Final premium due date
                                </label>
                                <span className="desc-text">{policyDetailsObject.PolicyDetails.FINAL_PREMIUM_DUE}</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Payment Method
                                </label>
                                <span className="desc-text">{policyDetailsObject.PolicyDetails.PAYMENT_METHOD}</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Premium Mode
                                </label>
                                <span className="desc-text">{policyDetailsObject.PolicyDetails.PREMIUM_MODE}</span>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </ExpansionPanelDetails>
                  </ExpansionPanel>

                  {policyDetailsObject.PolicyDetails.ANNUITY_AMOUNT && 
                    <ExpansionPanel square className="mat-accordion">
                    <ExpansionPanelSummary>
                      <div className="accor-head">
                        <h3 className="accor-title">Annuity Details</h3>
                        <div className="accor-act">
                          <button className="btn btn-iconText" onClick={preventExpanding}>
                            <span className="icon icon-edit"></span>
                            <span className="text">Edit</span>
                          </button>
                        </div>
                        <span className="icon icon-chevron-down"></span>
                      </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                      <div className="accor-body">
                        <div className="m-detail-list">
                          <ul className="row">
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Purchase Price
                                </label>
                                <span className="desc-text">
                                  &#8377; 1,00,00,000
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Annuity Amount 
                                </label>
                                <span className="desc-text">
                                  &#8377; 1,00,000
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Annuity Frequency
                                </label>
                                <span className="desc-text">
                                  Monthly
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Next Payout Date
                                </label>
                                <span className="desc-text">20 May 2020</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Annuity Option
                                </label>
                                <span className="desc-text">Option 3</span>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                  }
                  
                
                
                  <ExpansionPanel square className="mat-accordion">
                    <ExpansionPanelSummary>
                      <div className="accor-head">
                        <h3 className="accor-title">Personal Details</h3>
                        <div className="accor-act">
                          <button className="btn btn-iconText" onClick={preventExpanding}>
                            <span className="icon icon-edit"></span>
                            <span className="text">Edit</span>
                          </button>
                        </div>
                        <span className="icon icon-chevron-down"></span>
                      </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                      <div className="accor-body">
                        <div className="m-detail-list">
                          <ul className="row">
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Policy Owner Name
                                </label>
                                <span className="desc-text">
                                  {policyDetailsObject.PolicyDetails.POLICY_OWNER_NAME}
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Policy Owner DOB
                                </label>
                                <span className="desc-text">{policyDetailsObject.PolicyDetails.POLICY_OWNER_DOB}</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Registered mobile number
                                </label>
                                <span className="desc-text">
                                  {policyDetailsObject.PolicyDetails.REGISTERED_MOBILE_NO}
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Registered email ID
                                </label>
                                <span className="desc-text">
                                  {policyDetailsObject.PolicyDetails.REGISTERED_MOBILE_NO}
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">PAN Number</label>
                                <span className="desc-text">{policyDetailsObject.PolicyDetails.PAN_NUMBER}</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Aadhar Number
                                </label>
                                <span className="desc-text">
                                  {policyDetailsObject.PolicyDetails.AADHAR_NUMBER}
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Communication address
                                </label>
                                <div className="desc-text">
                                  <address>
                                    {policyDetailsObject.PolicyDetails.CURRENT_ADDRESS}
                                  </address>
                                </div>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Permanent address
                                </label>
                                <div className="desc-text">
                                  <address>
                                    {policyDetailsObject.PolicyDetails.PERMANENT_ADDRESS}
                                  </address>
                                </div>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                WhatsApp Opt In Status
                                </label>
                                <span className="desc-text">
                                {policyDetailsObject.PolicyDetails.WHATSAPP_IND}
                                </span>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                
                
                  <ExpansionPanel square className="mat-accordion">
                    <ExpansionPanelSummary>
                      <div className="accor-head">
                        <h3 className="accor-title">Nominee Details</h3>
                        <div className="accor-act">
                          <button className="btn btn-iconText" onClick={() => props.history.push('/updatenominee')}>
                            <span className="icon icon-edit"></span>
                            <span className="text">Edit</span>
                          </button>
                        </div>
                        <span className="icon icon-chevron-down"></span>
                      </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                      <div className="accor-body">
                        <div className="m-detail-list">
                          <div className="detail-head">
                            <h4 className="detail-title">Nominee 01</h4>
                          </div>
                          <ul className="row">
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">Name</label>
                                <span className="desc-text">{policyDetailsObject.NomineeDetails[0].NAME}</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">Allocation</label>
                                <span className="desc-text">{policyDetailsObject.NomineeDetails[0].ALLOCATION}</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Relationship with insured
                                </label>
                                <span className="desc-text">{policyDetailsObject.NomineeDetails[0].RELATION_WITH_INSURED}</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Date of Birth
                                </label>
                                <span className="desc-text">{policyDetailsObject.NomineeDetails[0].DATE_OF_BIRTH_FOR_APPOINTEE}</span>
                              </div>
                            </li>
                          </ul>
                        </div>
                        <div className="m-detail-list">
                          <div className="detail-head">
                            <h4 className="detail-title">Nominee 02</h4>
                          </div>
                          <ul className="row">
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">Name</label>
                                <span className="desc-text">Rahul Roy</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">Allocation</label>
                                <span className="desc-text">50%</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Relationship with insured
                                </label>
                                <span className="desc-text">Son</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Date of Birth
                                </label>
                                <span className="desc-text">19/01/1960</span>
                              </div>
                            </li>
                          </ul>
                          <ul className="row">
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Appointee name
                                </label>
                                <span className="desc-text">Ria Roy</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Relationship with insured
                                </label>
                                <span className="desc-text">Sister</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Date of Birth
                                </label>
                                <span className="desc-text">19/01/1991</span>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                
                
                  <ExpansionPanel square className="mat-accordion">
                    <ExpansionPanelSummary>
                      <div className="accor-head">
                        <h3 className="accor-title">Past Payment Details</h3>
                        <div className="accor-act">
                          <button className="btn btn-iconText" onClick={preventExpanding}>
                            <span className="icon icon-edit"></span>
                            <span className="text">Edit</span>
                          </button>
                        </div>
                        <span className="icon icon-chevron-down"></span>
                      </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                      <div className="accor-body">
                          <PastPaymentsTable1/>
                      </div>
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                
                
                  <ExpansionPanel square className="mat-accordion">
                    <ExpansionPanelSummary>
                      <div className="accor-head">
                        <h3 className="accor-title">Bank Account Details</h3>
                        <div className="accor-act">
                          <button className="btn btn-iconText" onClick={() => props.history.push("/bank-detail")}>
                            <span className="icon icon-edit"></span>
                            <span className="text">Edit</span>
                          </button>
                        </div>
                        <span className="icon icon-chevron-down"></span>
                      </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                      <div className="accor-body">
                        <div className="m-detail-list">
                          <ul className="row">
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Account Holder Name
                                </label>
                                <span className="desc-text">{policyDetailsObject.PolicyDetails.ACCOUNT_HOLDER_NAME}</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">Bank Name</label>
                                <span className="desc-text">{policyDetailsObject.PolicyDetails.NEFT_BNK_NM}</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Branch name
                                </label>
                                <span className="desc-text">
                                  {policyDetailsObject.PolicyDetails.NEFT_BNK_BR_NM}
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">IFSC Code</label>
                                <span className="desc-text">{policyDetailsObject.PolicyDetails.NEFT_IFSC_CD}</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Account number
                                </label>
                                <span className="desc-text">
                                {policyDetailsObject.PolicyDetails.NEFT_ACCT_NUM}
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Account type
                                </label>
                                <span className="desc-text">{policyDetailsObject.PolicyDetails.NEFT_ACCT_TYP_CD}</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">MICR Code</label>
                                <span className="desc-text">{policyDetailsObject.PolicyDetails.NEFT_BNK_MICR_CD}</span>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                
                
                  <ExpansionPanel square className="mat-accordion">
                    <ExpansionPanelSummary>
                      <div className="accor-head">
                        <h3 className="accor-title">Agent Details</h3>
                        <span className="icon icon-chevron-down"></span>
                      </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                      <div className="accor-body">
                        <div className="m-detail-list">
                          <ul className="row">
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Advisor name
                                </label>
                                <span className="desc-text">
                                  {policyDetailsObject.PolicyDetails.ADVISORNAME}
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Mobile number
                                </label>
                                <span className="desc-text">
                                  {policyDetailsObject.PolicyDetails.REGISTERED_MOBILE_NO}
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">Email Id</label>
                                <span className="desc-text">
                                  {policyDetailsObject.PolicyDetails.REGISTERED_EMAIL_ID}
                                </span>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                
                
                  <ExpansionPanel square className="mat-accordion">
                    <ExpansionPanelSummary>
                      <div className="accor-head">
                        <h3 className="accor-title">Loan Availed</h3>
                        <div className="accor-act">
                          <button className="btn btn-link" onClick={preventExpanding}>
                            <span className="text">Pay now</span>
                          </button>
                        </div>
                        <span className="icon icon-chevron-down"></span>
                      </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                      <div className="accor-body">
                        <div className="m-detail-list">
                          <ul className="row">
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Total loan amount
                                </label>
                                <span className="desc-text">
                                  &#8377; {policyDetailsObject.LoanDetails.Total_Loan_Amount}
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Outstanding loan amount
                                </label>
                                <span className="desc-text">
                                  &#8377; 30,000,000
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Current rate of interest
                                </label>
                                <span className="desc-text">
                                  9.3% per annum
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                Installment
                                </label>
                                <span className="desc-text">
                                &#8377; 30,000/month
                                </span>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                
                
                  <ExpansionPanel square className="mat-accordion">
                    <ExpansionPanelSummary>
                      <div className="accor-head">
                        <h3 className="accor-title">Loan Eligibility</h3>
                        <div className="accor-act">
                          <button className="btn btn-link" onClick={preventExpanding}>
                            <span className="text">Apply for Loan</span>
                          </button>
                        </div>
                        <span className="icon icon-chevron-down"></span>
                      </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                      <div className="accor-body">
                        <div className="m-detail-list">
                          <ul className="row">
                            <li className="col-md-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Loan Eligibility
                                </label>
                                <span className="desc-text">
                                  Congratulations. Your Policy is eligible for
                                  loan amount of 10,00,000
                                </span>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                
                
                  <ExpansionPanel square className="mat-accordion">
                    <ExpansionPanelSummary>
                      <div className="accor-head">
                        <h3 className="accor-title">Fund Details</h3>
                        <div className="accor-act">
                          <button className="btn btn-iconText" onClick={preventExpanding}>
                            <span className="icon icon-edit"></span>
                            <span className="text">Edit</span>
                          </button>
                        </div>
                        <span className="icon icon-chevron-down"></span>
                      </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                      <div className="accor-body">
                        <div className="m-detail-list">
                          <ul className="row">
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Investor Profile
                                </label>
                                <span className="desc-text">
                                  Equity Funds
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Total fund value
                                </label>
                                <span className="desc-text">
                                  &#8377; 30,000,000
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Growth rate
                                </label>
                                <span className="desc-text">9.3%</span>
                              </div>
                            </li>
                          </ul>
                        </div>
                        <div className="m-detail-list">
                          <div className="detail-head">
                            <h4 className="detail-title">Fund 01</h4>
                            <span className="detail-meta">
                              As on 23 Mar 2020
                            </span>
                          </div>
                          <ul className="row">
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">Name</label>
                                <span className="desc-text">Swati Roy</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">NAV</label>
                                <span className="desc-text">12.32</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">Fund value</label>
                                <span className="desc-text">
                                  &#8377; 30,000,000
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">Allocation</label>
                                <span className="desc-text">50%</span>
                              </div>
                            </li>
                          </ul>
                        </div>
                        <div className="m-detail-list">
                          <div className="detail-head">
                            <h4 className="detail-title">Fund 02</h4>
                            <span className="detail-meta">
                              As on 23 Mar 2020
                            </span>
                          </div>
                          <ul className="row">
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">Name</label>
                                <span className="desc-text">Swati Roy</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">NAV</label>
                                <span className="desc-text">12.32</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">Fund value</label>
                                <span className="desc-text">
                                  &#8377; 30,000,000
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">Allocation</label>
                                <span className="desc-text">50%</span>
                              </div>
                            </li>
                          </ul>
                        </div>
                        <div className="m-detail-list">
                          <div className="detail-head">
                            <h4 className="detail-title">Fund 03</h4>
                            <span className="detail-meta">
                              As on 23 Mar 2020
                            </span>
                          </div>
                          <ul className="row">
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">Name</label>
                                <span className="desc-text">Swati Roy</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">NAV</label>
                                <span className="desc-text">12.32</span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-6">
                              <div className="m-lbl-text">
                                <label className="lbl-text">Fund value</label>
                                <span className="desc-text">
                                  &#8377; 30,000,000
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">Allocation</label>
                                <span className="desc-text">50%</span>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                
                
                  <ExpansionPanel square className="mat-accordion">
                    <ExpansionPanelSummary>
                      <div className="accor-head">
                        <h3 className="accor-title">
                          Bonus &amp; Benefit Details
                        </h3>
                        <span className="icon icon-chevron-down"></span>
                      </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                      <div className="accor-body">
                        <div className="m-detail-list">
                          <ul className="row">
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Total accumulated bonus
                                </label>
                                <span className="desc-text">
                                  &#8377; 30,000,000
                                </span>
                              </div>
                            </li>
                            <li className="col-md-4 col-xs-12">
                              <div className="m-lbl-text">
                                <label className="lbl-text">
                                  Guaranteed Benefit
                                </label>
                                <span className="desc-text">
                                  &#8377; 1,25,65,777
                                </span>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                
                </div>
            </div>
          </div>
          {scrollBtnState?
          <button className="btn btn-icon btn-scrooltop desktop" onClick={scrollTopHandler} style={{'right':(scrollBtnPosition.right+50)+'px'}}>
            <span className="icon icon-chevron-up"></span>
          </button>:''
          }
          {/* page template end here */}
          </div>
        </div>
        <div className={`modal ${(ABCidModalState)?'active':''}`}>
            <div className="dialog">
            <div className="content">
                <div className="modal-header">
                    <h5 className="title">Congratulations</h5>
                    <button type="button" className="btn btn-icon" onClick={closeModalHandler}>
                        <span className="icon icon-cancel"></span>
                    </button>
                </div>
                <div className="modal-body">
                    <p className="para">You are eligible for loan amount of &#8377; 10,000</p>
                </div>
                <div className="act-btn">
                <button type="button" className="btn" >Apply for Loan</button>
                </div>
            </div>
            </div>
        </div>
        
        <span className={`overlay ${(ABCidModalState)?'active':''}`}></span>
      </div>
      {/* <Footer /> */}

      <FloatingMenu />
      <Loader show={loader} />
    </>
  );
}

const mapstateToProps = (state) => {
  console.log('mapstate',state);
  return {
    policydetails : state.policydetails
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getPolicyDetails : data => dispatch(getPolicyDetails(data))
  }
}

export default connect(mapstateToProps,mapDispatchToProps)(PolicyDetail);
