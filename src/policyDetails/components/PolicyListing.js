import React, { useEffect, useState } from "react";
import { isMobile } from "react-device-detect";
import Swiper from "swiper";

import SideNav from "../../home/containers/sideNav";
import { NavLink } from "react-router-dom";
import PolicyPlanCard from "../containers/PolicyPlanCard";
import QuickActions from "../containers/QuickActions";
import { connect } from 'react-redux';
import axios from 'axios';

const PolicyListing = (props) => {
  const [className, setClassName] = useState("");
  const [sidebarMobileState, setSidebarMobileState] = useState(false);
  const [viewFlag, setViewCheckboxFlag] = useState("");
  const [viewClassName, setViewClassName] = useState("");
  const [ABCidModalState,setABCidModalState] = useState(true)
  const [dashboardData, setdashboardData] = useState("");

  let mIconTextswiper;

useEffect(() => {
  axios.post('http://localhost:8080/api/getDashboardDetails')
    .then((res) => {
      // console.log('res',res.data.data.DASHBOARDdDETAILS)
      setdashboardData(res.data.data.DASHBOARDdDETAILS)
    })
    .catch((err) => console.log('err'))
},[])

  const closeModalHandler = ()=>{
    setABCidModalState(false);
  }

  const sideBarHandlerMobile = () => {
    if (sidebarMobileState) {
      setSidebarMobileState(false);
    } else {
      setSidebarMobileState(true);
    }
  };

  const handleChange = (e) => {
    if(e.target.checked){
      setViewClassName("list-view")
      setViewCheckboxFlag("checked");
    }else{
      setViewClassName("")
      setViewCheckboxFlag("");
    }
  }

  useEffect(() => {
    // console.log('use effect called')
    if(!isMobile) {
      setViewCheckboxFlag("checked");
      setViewClassName("grid-view")
    }

    mIconTextswiper = new Swiper(".js-filter-swiper", {
      speed: 400,
      slidesPerView: "auto",
      breakpoints: {
      1025: {
          slidesPerView: 7,
        },
      },
    });
  }, []);

const getPolicyListing = (policyNumber) => {
  //get policy details with policy number
  let policy_id = policyNumber;
  const effective_date = "21-06-2020";
  const source = '4170703531';
  const data = {policy_id,effective_date,source}
  // console.log(data)
  //navigate to policy details page with policy details data
  props.history.push('/policy-detail',data);
  // props.handleRouting(data)
}

  return (
    <>
      {/* <Header /> */}
      <div className="container-fluid refContainer">
        <SideNav docClass={className} openStateMobile={sidebarMobileState} />
        <div className="content-wrapper">
            {/* action header start */}
            <div className="bs-action-header">
                  {isMobile ? (
                      <h1 className="title" onClick={sideBarHandlerMobile}>
                          Policies and Details
                          <span className="icon icon-chevron-down"></span>
                      </h1>
                  ) : (
                          <h1 className="title">
                              Policies and Details
                              <span className="icon icon-chevron-down"></span>
                          </h1>
                      )}
                  <QuickActions />
            </div>
            {/* action header end */}
    
            {/* breadcrumb starts */}
              <ol className="m-breadcrumb">
                  <li className="item">
                      <NavLink to="" className="link" title="Home">
                          Home<span className="icon icon-chevron-right"></span>
                      </NavLink>
                  </li>
                  <li className="item">Policies and Details</li>
              </ol>
            {/* breadcrumb end */}

            <div className="m-filter swiper-container js-filter-swiper">
                <ul className="list swiper-wrapper">
                <li className="item swiper-slide active">
                    <NavLink className="tab" to="#" title="All Plans">
                    All Plans
                    </NavLink>
                </li>
                <li className="item swiper-slide">
                    <NavLink className="tab" to="#" title="Term">
                    Term Plans
                    </NavLink>
                </li>
                <li className="item swiper-slide">
                    <NavLink className="tab" to="#" title="Health">
                    Health Plans
                    </NavLink>
                </li>
                <li className="item swiper-slide">
                    <NavLink className="tab" to="#" title="Annuity">
                    Annuity Plans
                    </NavLink>
                </li>
                <li className="item swiper-slide">
                    <NavLink className="tab" to="#" title="ULIP">
                    ULIP Plans
                    </NavLink>
                </li>
                <li className="item swiper-slide">
                    <NavLink className="tab" to="#" title="Participating">
                    Participating Plans
                    </NavLink>
                </li>
                <li className="item swiper-slide">
                    <NavLink
                    className="tab"
                    to=""
                    title="Non Participating"
                    >
                    Non Participating Plans
                    </NavLink>
                </li>
                </ul>
            </div>

            <div className="m-switch-wrap desktop">
                <label for="view" className="name">Grid View</label>
                <label for="view" className="switch">
                    <input className="switch-input" type="checkbox" checked={viewFlag} onChange={handleChange} id="view" />
                    <span className="slider round"></span>
                </label>
                <label for="view" className="name">List View</label>
            </div>
            <ul className={`lyt-plans ${viewClassName}`}>
              {/* {props.dashBoard &&
                    props.dashBoard.DASHBOARDdDETAILS.map((policy,index) => {
                      return(
                        <li className="plans-item" key={policy.POLICY_NUMBER}>
                          <PolicyPlanCard 
                          data ={ policy }
                          finance={ props.dashBoard.FUNDDETAILS}
                          getPolicyListing={getPolicyListing}
                          />
                        </li>
                      )
                    })} */}
                    {dashboardData && dashboardData.map((policy,index) => {
                      return(
                        <li className="plans-item" key={policy.POLICY_NUMBER}>
                        <PolicyPlanCard 
                        data ={ policy }
                        finance={ dashboardData.FUNDDETAILS}
                        getPolicyListing={getPolicyListing}
                        />
                      </li>
                      )
                    })}
            </ul>
        </div>

        <div className={`modal ${(ABCidModalState)?'active':''}`}>
            <div className="dialog">
            <div className="content">
                <div className="modal-header">
                    <h5 className="title">Missing details</h5>
                    <button type="button" className="btn btn-icon" onClick={closeModalHandler}>
                        <span className="icon icon-cancel"></span>
                    </button>
                </div>
                <div className="modal-body">
                    <p className="para">Following important details are missing, please help us by filling up the details to avail benefits</p>
                    <ul className="actList">
                      <li className="actItem">
                        <button className="btn btn-link">Survival Certificate</button>
                      </li>
                      <li className="actItem">
                        <button className="btn btn-link">Nominee Details</button>
                      </li>
                      <li className="actItem">
                        <button className="btn btn-link">Bank Details</button>
                      </li>
                    </ul>
                </div>
            </div>
            </div>
        </div>
        <span className={`overlay ${(ABCidModalState)?'active':''}`}></span>
      </div>
      {/* <Footer /> */}
    </>
  );
};

const mapstateToProps = (state) => {
  // console.log('mapstate',state);
  return {
    dashBoard : state.home.dashboard
  }
}

export default connect(mapstateToProps)(PolicyListing);
