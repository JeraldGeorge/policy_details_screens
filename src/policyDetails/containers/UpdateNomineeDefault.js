import React, { useEffect, useState } from "react";
import { isMobile } from "react-device-detect";
import Header from "../components/Header";
import Footer from "../components/Footer";
import SideNav from "../../home/containers/sideNav";
import FloatingMenu from "./FloatingMenu";
import Loader from "../../home/containers/loader";
import { NavLink } from "react-router-dom";
import QuickActions from "./QuickActions";

function UpdateNomineeDefault() {
    const [loader, setLoader] = useState(true);
    const [sidebarMobileState, setSidebarMobileState] = useState(false);
    const [ABCidModalState,setABCidModalState] = useState(true)

    useEffect(() => {
        setLoader(false);
    }, []);


    const sideBarHandlerMobile = () => {
        if (sidebarMobileState) {
            setSidebarMobileState(false);
        } else {
            setSidebarMobileState(true);
        }
    };

    const closeModalHandler = ()=>{
        setABCidModalState(false);
    }

    return (
        <>
            <Header />
            <div className="container-fluid refContainer">
                <SideNav openStateMobile={sidebarMobileState} />
                <div className="content-wrapper">
                    {/* action header start */}
                    <div className="bs-action-header">
                        {isMobile ? (
                            <h1 className="title" onClick={sideBarHandlerMobile}>
                                Update Nominee
                                <span className="icon icon-chevron-down"></span>
                            </h1>
                        ) : (
                                <h1 className="title">
                                    Update Nominee
                                    <span className="icon icon-chevron-down"></span>
                                </h1>
                            )}
                        <QuickActions />
                    </div>
                    {/* action header end */}
                    {/* breadcrumb starts */}
                    <ol className="m-breadcrumb desktop">
                        <li className="item">
                            <NavLink to="" className="link" title="Home">
                                Home<span className="icon icon-chevron-right"></span>
                            </NavLink>
                        </li>
                        <li className="item">
                            <NavLink to="" className="link" title="Home">
                            Policies and Details<span className="icon icon-chevron-right"></span>
                            </NavLink>
                        </li>
                        <li className="item">Update Nominee</li>
                    </ol>
                    {/* breadcrumb end */}
                    {/* page template start here */}
                    <div className="lyt-nominee">
                        <div className="bs-section">
                            <div className="sec-cont">
                                <div className="row">
                                    <div className="col-md-2 col-xs-12">
                                        <button className="btn btn-iconText addbtn btn-disabled">
                                            <span className="icon icon-add"></span>
                                            <span className="btn-text">Add nominee</span>
                                        </button>
                                    </div>
                                </div>
                                <div className="list-wrap">
                                    <ul className="list">
                                        <li className="item row">
                                            <div className="col-md-4 col-xs-6">
                                                <div class="m-lbl-text">
                                                    <label class="lbl-text">Name</label>
                                                    <span class="desc-text">Rishi Roy</span>
                                                </div>
                                            </div>
                                            <div className="col-md-4 col-xs-6">
                                                <div className="form-group typ-error">
                                                    <label className="lbl">Allocation %</label>
                                                    <input type="text" className="form-control" value="50%" placeholder="Enter Allocation eg. 50%" />
                                                    <span className="error">Enter a valid percentage</span>
                                                </div>
                                            </div>
                                            <div className="col-md-4 col-xs-12 actBtn--right">
                                                <button className="btn btn-iconText delete">
                                                    <span className="icon icon-delete"></span>
                                                    <span className="text">Delete Nominee</span>
                                                </button>
                                            </div>
                                        </li>
                                        <li className="item row">
                                            <div className="col-md-4 col-xs-6">
                                                <div class="m-lbl-text">
                                                    <label class="lbl-text">Name</label>
                                                    <span class="desc-text">Rishi Roy</span>
                                                </div>
                                            </div>
                                            <div className="col-md-4 col-xs-6">
                                                <div className="form-group">
                                                    <label className="lbl">Allocation %</label>
                                                    <input type="text" className="form-control" value="50%" placeholder="Enter Allocation eg. 50%" />
                                                    <span className="error">Enter a valid percentage</span>
                                                </div>
                                            </div>
                                            <div className="col-md-4 col-xs-12 actBtn--right">
                                                <button className="btn btn-iconText delete">
                                                    <span className="icon icon-delete"></span>
                                                    <span className="text">Delete Nominee</span>
                                                </button>
                                            </div>
                                        </li>
                                    </ul>
                                    <p className="relInfo">As per your policy details, you are not allowed to add new nominee / edit existing nominee details</p>
                                    <div className="act-btn text-right">
                                        <NavLink to="/updatenominee" class="btn btn-fill btn-disabled">Change Nominee Details</NavLink>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={`modal ${(ABCidModalState)?'active':''}`}>
                        <div className="dialog">
                        <div className="content">
                            <div className="modal-header">
                                <h5 className="title">Share Allocation Invalid</h5>
                                <button type="button" className="btn btn-icon" onClick={closeModalHandler}>
                                    <span className="icon icon-cancel"></span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <p className="para">Total share allocation of all nominees needs to be 100%.</p>
                                <p className="para">Please correct it to continue.</p>
                            </div>
                            <div className="act-btn">
                            <button type="button" className="btn btn-fill">Change Allocation</button>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <span className={`overlay ${(ABCidModalState)?'active':''}`}></span>
                    {/* page template end here */}
                </div>
            </div>
            <Footer />

            <FloatingMenu />
            <Loader show={loader} />

        </>
    );
}

export default UpdateNomineeDefault;
