import React,{useState} from 'react'
import useGetPositions from '../../hooks/useGetPositions';
import { NavLink } from 'react-router-dom';

function FloatingMenu() {
    const [mfloatingBtnState, setMfloatingBtnState] = useState(false);
    const [floatingBtnPosition] = useGetPositions('refContainer');
    const mFloatingMenuStateHandler=()=>{
        if(mfloatingBtnState){
            setMfloatingBtnState(false)
        }else{
            setMfloatingBtnState(true)
        }
    }
    return (
        <>
            <div className={`m-floating-btn ${(mfloatingBtnState)?' active':''}`} style={{'right':(floatingBtnPosition.right+40)+'px'}}>
                <ul className="list mobile">
                    <li className="item">
                        <button className="link" title="chat">
                            <span className="text">chat</span>
                            <span className="link-round round icon icon-chat"></span>
                        </button>
                    </li>
                    <li className="item">
                        <button className="link" title="Branch Locator">
                            <span className="text">Branch Locator</span>
                            <span className="link-round round icon icon-branch-locator"></span>
                        </button>
                    </li>
                    <li className="item">
                        <button className="link" title="Call">
                            <span className="text">Call</span>
                            <span className="link-round round icon icon-call"></span>
                        </button>
                    </li>
                </ul>
                <button title="Icon Cancel" onClick={mFloatingMenuStateHandler} className="mobile round">
                    <span className="mobile icon icon-cancel close"></span>
                </button>
                <button title="Icon Chat" onClick={mFloatingMenuStateHandler} className="desktop floating-btn-desktop round">
                    <span className="desktop icon icon-chat"></span>
                </button>
            </div>
            <span className={`overlay ${(mfloatingBtnState)?'active':''} mobile`}></span>
        </>
    )
}

export default FloatingMenu
