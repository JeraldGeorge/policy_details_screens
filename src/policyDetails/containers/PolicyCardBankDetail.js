import React, { useState, useEffect } from 'react';
// import InfoHelp from './InfoHelp';

function PolicyCardBankDetail() {
    const [payPolicyCardState, setPayPolicyCardState] = useState(false);
    const [showEditableFld, setShowEditableFld] = useState(false);

    useEffect(() => {

    })
    const payPolicyCardStateHandler = () => {
        setPayPolicyCardState(!payPolicyCardState)
    }
    const customisedAmountHandler = (e) => {
        console.log(e.currentTarget.checked)
        if (e.currentTarget.value == 'customValue') {
            setShowEditableFld(true)
        } else {
            setShowEditableFld(false)
        }

    }
    return (
        <>
            <div className={`m-policy-card ${(payPolicyCardState) ? 'expanded' : ''} typ-bank-detail`}>
                <div className="head" onClick={payPolicyCardStateHandler}>
                    <div className="row">
                        <div className="col-sm-3">
                            <div className="m-lbl-text">
                                <label className="lbl-text">Policy No.</label>
                                <span className="desc-text">726183618</span>
                            </div>
                        </div>
                        <div className="col-sm-3">
                            <div className="m-lbl-text">
                                <label className="lbl-text">Plan Name</label>
                                <span className="desc-text">ABSLI Digishield</span>
                            </div>
                        </div>
                        <div className="col-sm-6">


                            <div className="m-lbl-text">
                                <label className="lbl-text">Bank Details</label>
                                <span className="desc-text"><span className="tag active">Available</span></span>
                            </div>

                        </div>
                    </div>

                    <button className="btn btn-icon" ><span className="icon icon-chevron-down"></span></button>

                </div>
                <div className="body">

                    <div className="content">
                        <h2 className="card-title">
                            Bank Details
                            <button className="btn btn-iconText">
                                <span className="icon icon-edit"></span>
                                <span className="text">Edit</span>
                            </button>
                        </h2>
                        <div className="row">

                            <div className="col-sm-3">
                                <div className="m-lbl-text">
                                    <label className="lbl-text">Account Number</label>
                                    <span className="desc-text">123456789</span>
                                </div>
                            </div>
                            <div className="col-sm-3">
                                <div className="m-lbl-text">
                                    <label className="lbl-text">Bank Name</label>
                                    <span className="desc-text">HDFC Bank</span>
                                </div>
                            </div>

                            <div className="col-sm-6">
                                <div className="m-lbl-text">
                                    <label className="lbl-text">Branch Name</label>
                                    <span className="desc-text">Mumbai Worli Sea-face</span>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default PolicyCardBankDetail
