import React,{useEffect,useRef,useState} from "react";
import { NavLink } from "react-router-dom";
import PlanCardProgress from "./PlanCardProgress";
import PolicyListing from "../components/PolicyListing";

const PolicyPlanCard = (props) => {
  const node = useRef();
  const [kebabMenuState,setKebabMenuState] = useState(false)
  const [GridViewState,setGridViewState] = useState(false)
  const handleClickOutside = (e)=>{
    if (node.current.contains(e.target)) {
      // inside click
      return;
    }
    setKebabMenuState(false);
    // outside click 
  }

  const showHideDetails = (event) => {
    // console.log(!event.target.classList.contains('icon-kebab-menu'));
    if(!event.target.classList.contains('icon-kebab-menu'))
      setGridViewState(!GridViewState);
    // // console.log(event.currentTarget);
    // if (event.currentTarget.parentNode.classList.contains('planCard--show')) {
    //     event.currentTarget.parentNode.classList.remove('planCard--show')
    // } else {
    //     event.currentTarget.parentNode.classList.add('planCard--show');
    // }
  }

  useEffect(() => {
    // add when mounted
    document.addEventListener("click", handleClickOutside);
    // return function to be called when unmounted
    return () => {
      document.removeEventListener("click", handleClickOutside);
    };
  }, []);

  const setKebabMenuStateHandler=()=>{
    setKebabMenuState(!kebabMenuState);
  }

  // console.log('policy card',props.data)

const passPolicyListing = (policyNumber) => {
  // console.log('passPolicyListing called',policyNumber)
  props.getPolicyListing(policyNumber);
}

let currentDate = new Date();
let paymentDueDate = new Date("2020/08/01");
let diff = Math.abs(currentDate-paymentDueDate);
let toDays = (diff / (60*60*24*1000))
let days = parseInt(toDays);


  return (
    <>
      <div className={`m-plan-card policy ${(GridViewState)?'show':''}`}>
        <div className="head" onClick={showHideDetails}>
          <ul className="list">
            <li className="item">
              <div className="m-lbl-text">
                <label className="lbl-text">Policy No.</label>
                <span className="desc-text">{props.data.POLICY_NUMBER} <span className="accorArrow  icon icon-chevron-down"></span></span>
              </div>
            </li>
            <li className="item">
              <div className="m-lbl-text">
                <label className="lbl-text">Policy Type</label>
                <span className="desc-text">
                  {props.data.POLICY_TYPE}
                </span>
              </div>
            </li>
            <li className="item">
              <div className="m-lbl-text">
                <label className="lbl-text">Plan Type</label>
                <span className="desc-text">
                  {props.data.PLAN_TYPE}
                </span>
              </div>
            </li>
          </ul>

          <span className="tag active">Active</span>

          <div className={`dote-menu ${(kebabMenuState)?'active':''}`}>
            <span ref={node} className="icon icon-kebab-menu" onClick={setKebabMenuStateHandler}></span>
            <ul  className="menu">
              <li className="item">
                <NavLink className="link" to="" title="Download statement">
                  Download statement
                </NavLink>
              </li>
              {props.data.POLICY_TYPE === "Pre Ulip" && 
                <li className="item">
                  <NavLink className="link" to="" title="Switch funds">
                    Switch funds
                  </NavLink>
                </li>
              }
              {/* <li className="item">
                <NavLink className="link" to="" title="Change registered address">
                  Change registered address
                </NavLink>
              </li> */}
              <li className="item">
                <NavLink className="link" to="" title="Set Alerts">
                  Set Alerts
                </NavLink>
              </li>
              {props.data.POLICY_TYPE === "Annuity" && 
                <li className="item">
                  <NavLink className="link" to="" title="Update bank details">
                    Update bank details
                  </NavLink>
                </li>
              }
            </ul>
          </div>

        </div>
        <div className="cont">
          <ul className="list">
            <li className="item">
              <div className="m-lbl-text">
                <label className="lbl-text">Policy Owner Name</label>
                <span className="desc-text">Akshay Nirav Khanna</span>
              </div>
            </li>
            <li className="item">
              <div className="m-lbl-text">
                <label className="lbl-text">Insured for</label>
                <span className="desc-text">&#8377; {props.data.TOTAL_SUM_ASSURED}</span>
              </div>
            </li>
            <li className="item">
              <div className="m-lbl-text">
                <label className="lbl-text">Till the age of</label>
                <span className="desc-text">{props.data.POLICY_TERM}</span>
              </div>
            </li>
            <li className="item view">
              <div className="m-lbl-text">
                <label className="lbl-text">Premium of</label>
                <span className="desc-text">&#8377; {props.data.POLICYPREMIUM}<span>/year</span></span>
              </div>
            </li>
            <li className="item view">
              <div className="m-lbl-text">
                <label className="lbl-text">Premium due on</label>
                <span className="desc-text">{props.data.NDD}</span>
              </div>
            </li>
            <li className="item item-full">
              <div className="m-lbl-text">
                <label className="lbl-text">Purpose of Insurance</label>
                <span className="tags">
                    <span className="tag">Family Protection</span>
                    <span className="tag">Tax Benefit</span>
                    <span className="tag">Risk Cover</span>
                </span>
              </div>
            </li>
          </ul>
          <div className="act-btn normal-view">
            <button className="btn btn-outline" type="button" onClick={() => passPolicyListing(props.data.POLICY_NUMBER)}>
                View Details
            </button>
            {days <= 30 ? (
              <button className="btn" type="button">
                Pay Premium
              </button>
            ) : (
              <button className="btn btn-outline" type="button">
               Download Receipt
              </button>
            ) 
            }
            <button type="button" className="btn btn-link">
                Download Tax Statement
            </button>
          </div>
          <div className="act-btn list-view">
              <button className="btn btn-outline" type="button">
                  Download Receipt
              </button>
              <button className="btn" type="button" onClick={() => passPolicyListing(props.data.POLICY_NUMBER)}>
                  View Details
              </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default PolicyPlanCard;
