import React,{useState} from 'react';
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  DatePicker,
  KeyboardDatePicker
} from "@material-ui/pickers";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import SelectDownArrow from "../icons/SelectDownArrow";
import { NavLink } from "react-router-dom";
import Upload from './Upload';

const AddNominee = () => {
    const [selectedDate, setSelectedDate] = useState(null);
    const [selectedDate2, setSelectedDate2] = useState(null);
    const [relationName, setRelation] = useState("relationName1");
    const [relationName2, setRelation2] = useState("relationName1");
    const handleDateChange = (date) => {
        setSelectedDate(date);
    };
    const handleDateChange2 = (date) => {
        setSelectedDate2(date);
    };
    const relationHandler = (event) => {
        console.log(event);
        setRelation(event.target.value);
    }
    const relationHandler2 = (event) => {
        console.log(event);
        setRelation2(event.target.value);
    }
    return (
        <div className="addnominee">
            <h2 className="title">Add Nominee</h2>
            <form>
                <ul className="row">
                    <li className="col-md-4 col-xs-12">
                        <div className="form-group">
                            <label className="lbl">Name</label>
                            <input
                            type="text"
                            className="form-control"
                            placeholder="Enter Nominee’s Full Name"
                            />
                            <span className="error">
                            Please check the Policy No./Application No. and try again
                            </span>
                        </div>
                    </li>
                    <li className="col-md-4 col-xs-6">
                        <div className="form-group show-error">
                            <label className="lbl">Allocation %</label>
                            <input
                            type="text"
                            className="form-control"
                            />
                            <span className="error">
                            Enter a valid percentage
                            </span>
                        </div>
                    </li>
                    <li class="clearfix"></li>
                    <li className="col-md-4 col-xs-6">
                        <div className="form-group">
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                autoOk
                                disableFuture
                                inputVariant="standard"
                                openTo="year"
                                format="dd/MM/yyyy"
                                InputLabelProps={{
                                shrink: true,
                                }}
                                label="Date of Birth"
                                placeholder="DD/MM/YYYY"
                                views={["year", "month", "date"]}
                                value={selectedDate}
                                onChange={handleDateChange}
                                keyboardIcon={''}
                                helperText={''}
                            />
                            </MuiPickersUtilsProvider>
                            <span className="error">
                            Please check the Policy No./Application No. and try again
                            </span>
                        </div>
                    </li>
                    <li className="col-md-4 col-xs-12">
                        <div className="form-group">
                            <label className="lbl">Relationship with Insurer</label>
                            <Select
                                labelId="demo-simple-select-placeholder-label-label"
                                id="demo-simple-select-placeholder-label"
                                value={relationName}
                                onChange={relationHandler}
                                placeholder="Select"
                                displayEmpty
                                MenuProps={{
                                    getContentAnchorEl: null,
                                    anchorOrigin: {
                                        vertical: "bottom",
                                        horizontal: "left",
                                    }
                                }}
                                IconComponent={SelectDownArrow}
                            >
                                <MenuItem value="relationName1" name="relationName1">Child</MenuItem>
                                <MenuItem value="relationName2" name="relationName2">Parent</MenuItem>
                                <MenuItem value="relationName3" name="relationName3">Grandparent</MenuItem>
                                <MenuItem value="relationName4" name="relationName4">Spouse</MenuItem>
                                <MenuItem value="relationName5" name="relationName5">Sibling</MenuItem>
                            </Select>
                            <span className="error">
                            Please check the Policy No./Application No. and try again
                            </span>
                        </div>
                    </li>
                </ul>
                <div className="appointee">
                    <h3 className="subtitle">Appointee</h3>
                    <ul className="row">
                        <li className="col-md-4 col-xs-12">
                            <div className="form-group">
                                <label className="lbl">Appointee Name</label>
                                <input
                                type="text"
                                className="form-control"
                                placeholder="Enter Nominee’s Full Name"
                                />
                                <span className="error">
                                Please check the Policy No./Application No. and try again
                                </span>
                            </div>
                        </li>
                        <li className="col-md-4 col-xs-12">
                            <div className="form-group">
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <KeyboardDatePicker
                                    autoOk
                                    disableFuture
                                    inputVariant="standard"
                                    openTo="year"
                                    format="dd/MM/yyyy"
                                    InputLabelProps={{
                                    shrink: true,
                                    }}
                                    label="Date of Birth"
                                    placeholder="DD/MM/YYYY"
                                    views={["year", "month", "date"]}
                                    value={selectedDate2}
                                    onChange={handleDateChange2}
                                    keyboardIcon={''}
                                    helperText={''}
                                />
                                </MuiPickersUtilsProvider>
                                <span className="error">
                                Please check the Policy No./Application No. and try again
                                </span>
                            </div>
                        </li>
                        <li className="col-md-4 col-xs-12">
                            <div className="form-group">
                                <label className="lbl">Relationship with Insurer</label>
                                <Select
                                    labelId="demo-simple-select-placeholder-label-label"
                                    id="demo-simple-select-placeholder-label"
                                    value={relationName2}
                                    onChange={relationHandler2}
                                    placeholder="Select"
                                    displayEmpty
                                    MenuProps={{
                                        getContentAnchorEl: null,
                                        anchorOrigin: {
                                            vertical: "bottom",
                                            horizontal: "left",
                                        }
                                    }}
                                    IconComponent={SelectDownArrow}
                                >
                                    <MenuItem value="relationName1" name="relationName1">Spouse</MenuItem>
                                    <MenuItem value="relationName2" name="relationName2">Parent</MenuItem>
                                    <MenuItem value="relationName3" name="relationName3">Grandparent</MenuItem>
                                    <MenuItem value="relationName4" name="relationName4">Child</MenuItem>
                                    <MenuItem value="relationName5" name="relationName5">Sibling</MenuItem>
                                </Select>
                                <span className="error">
                                Please check the Policy No./Application No. and try again
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>
                <div className="upload">
                    <div className="head-wrap">
                        <h3 className="uploadtitle">Upload KYC Document</h3>
                        <p className="subtext">Either Driving License or Passport or Voter ID</p>
                    </div>
                    <div className="cont-wrap">
                        <Upload/>
                    </div>
                </div>
            </form>
        </div>
    );
}

export default AddNominee;
