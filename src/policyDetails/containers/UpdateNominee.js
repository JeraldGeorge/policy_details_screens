import React, { useEffect, useState } from "react";
import { isMobile } from "react-device-detect";
// import Header from "../components/Header";
// import Footer from "../components/Footer";
import SideNav from "../../home/containers/sideNav";
import FloatingMenu from "./FloatingMenu";
import Loader from "../../home/containers/loader";
import { NavLink } from "react-router-dom";
import QuickActions from "./QuickActions";
// import AddNominee from "./AddNominee";

function UpdateNominee() {
    const [loader, setLoader] = useState(true);
    const [sidebarMobileState, setSidebarMobileState] = useState(false);
    const [OTPModalState,setOTPModalState] = useState(true)
    const [ABCidModalState,setABCidModalState] = useState(false)

    useEffect(() => {
        setLoader(false);
    }, []);

    const closeModalHandler = ()=>{
        setABCidModalState(false);
    }

    const closeOTPModalHandler = ()=>{
        setOTPModalState(false);
    }

    const verifyHandler = () => {
        setOTPModalState(false);
        setABCidModalState(true);
    }

    const sideBarHandlerMobile = () => {
        if (sidebarMobileState) {
            setSidebarMobileState(false);
        } else {
            setSidebarMobileState(true);
        }
    };

    return (
        <>
            {/* <Header /> */}
            <div className="container-fluid refContainer">
                <SideNav openStateMobile={sidebarMobileState} />
                <div className="content-wrapper">
                    {/* action header start */}
                    <div className="bs-action-header">
                        {isMobile ? (
                            <h1 className="title" onClick={sideBarHandlerMobile}>
                                Update Nominee
                                <span className="icon icon-chevron-down"></span>
                            </h1>
                        ) : (
                                <h1 className="title">
                                    Update Nominee
                                    <span className="icon icon-chevron-down"></span>
                                </h1>
                            )}
                        <QuickActions />
                    </div>
                    {/* action header end */}
                    {/* breadcrumb starts */}
                    <ol className="m-breadcrumb">
                        <li className="item">
                            <NavLink to="/dashboard" className="link" title="Home">
                                Home<span className="icon icon-chevron-right"></span>
                            </NavLink>
                        </li>
                        <li className="item">
                            <NavLink to="/policylisting" className="link" title="Home">
                            Policies and Details<span className="icon icon-chevron-right"></span>
                            </NavLink>
                        </li>
                        <li className="item">
                            <NavLink
                                to="/policy-detail"
                                className="link"
                                title="Policies and Details"
                            >
                                ABSLI Digishield Plan
                                <span className="icon icon-chevron-right"></span>
                            </NavLink>
                            </li>
                        <li className="item">Update Nominee</li>
                    </ol>
                    {/* breadcrumb end */}
                    {/* page template start here */}
                    <div className="lyt-nominee">
                        <div className="bs-section">
                            <div className="sec-cont">
                                <div className="row">
                                    <div className="col-md-10 col-xs-12">
                                        <div class="note highlight">
                                            <div class="m-goal typ-error">
                                                <label class="goal-lbl">Total Share Allocation</label>
                                                <span class="status-bar">
                                                    <span class="total"></span>
                                                    <span class="achive"></span>
                                                </span>
                                                <span class="status"><span>100</span><span>%</span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-2 col-xs-12">
                                        <button className="btn btn-iconText addbtn">
                                            <span className="icon icon-add"></span>
                                            <span className="btn-text">Add nominee</span>
                                        </button>
                                    </div>
                                </div>
                                <div className="list-wrap edit">
                                    <ul className="list">
                                        <li className="item row">
                                            <div className="col-md-4 col-xs-7">
                                                <div class="m-lbl-text">
                                                    <label class="lbl-text">Name</label>
                                                    <span class="desc-text">Rishi Roy</span>
                                                </div>
                                            </div>
                                            <div className="col-md-4 col-xs-5">
                                                <div className="form-group show-error">
                                                    <label className="lbl">Allocation %</label>
                                                    <input type="text" className="form-control" value="50%" placeholder="Enter Allocation eg. 50%" />
                                                    <span className="error">Enter a valid percentage</span>
                                                </div>
                                            </div>
                                            <div className="col-md-4 col-xs-12 text-right">
                                                <button className="btn btn-iconText delete">
                                                    <span className="icon icon-delete"></span>
                                                    <span className="text">Delete Nominee</span>
                                                </button>
                                            </div>
                                        </li>
                                        <li className="item row">
                                            <div className="col-md-4 col-xs-7">
                                                <div class="m-lbl-text">
                                                    <label class="lbl-text">Name</label>
                                                    <span class="desc-text">Rishi Roy</span>
                                                </div>
                                            </div>
                                            <div className="col-md-4 col-xs-5">
                                                <div className="form-group">
                                                    <label className="lbl">Allocation %</label>
                                                    <input type="text" className="form-control" placeholder="Enter Allocation" />
                                                    <span className="error">Enter a valid percentage</span>
                                                </div>
                                            </div>
                                            <div className="col-md-4 col-xs-12 text-right">
                                                <button className="btn btn-iconText delete">
                                                    <span className="icon icon-delete"></span>
                                                    <span className="text">Delete Nominee</span>
                                                </button>
                                            </div>
                                        </li>
                                    </ul>
                                    {/* <AddNominee/> */}
                                    <div className="act-btn text-right">
                                        <button class="btn btn-outline" type="button">Cancel</button>
                                        <button class="btn" type="button">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* page template end here */}
                </div>
            </div>
            {/* popup */}

            <div className={`modal ${(OTPModalState)?'active':''}`}>
                <div className="dialog modal-otp">
                    <div className="content">
                        <div className="modal-header">
                            <h5 className="title">OTP Verification</h5>
                            <button type="button" className="btn btn-icon" onClick={closeOTPModalHandler}>
                            <span className="icon icon-cancel"></span>
                        </button>
                        </div>
                        <div className="modal-body">
                            <p className="para">We have sent an OTP to the registered mobile number 98******78 & Email ID ka****oy@g***com. Please enter it below to complete verification.</p>
                            <form> 
                                <div className="input-group otp typ-error">
                                    <label className="lbl">Enter OTP</label>
                                    <input type="text" className="form-control mobile" placeholder="Enter OTP" />
                                    <div className="otp-box desktop">
                                        <input type="text" className="form-control" />
                                        <input type="text" className="form-control" />
                                        <input type="text" className="form-control" />
                                        <input type="text" className="form-control" />
                                        <input type="text" className="form-control" />
                                        <input type="text" className="form-control" />
                                    </div>
                                    <div className="input-group-append">
                                        <span className="input-group-text">
                                            <a aria-current="page" className="btn btn-link btn-isabled active" title="Resend OTP" href="/">Resend OTP</a>
                                            <span className="count">00:59</span>
                                        </span>
                                    </div>
                                    <span className="error">Please enter correct OTP and try again</span>
                                </div>
                            </form>
                        </div>
                        <div className="act-btn">
                            <button type="button" className="btn" onClick={verifyHandler}>Verify</button>
                        </div>
                    </div>
                </div>
            </div>
            <div className={`modal ${(ABCidModalState)?'active':''}`}>
                <div className="dialog">
                    <div className="content">
                        <div className="modal-header">
                            <h5 className="title">Request Accepted</h5>
                            <button type="button" className="btn btn-icon" onClick={closeModalHandler}>
                                <span className="icon icon-cancel"></span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p className="para">Request No. 12345 has been generated for Add Nomiee on Policy No. 987654321 &amp; will get processed by 5th May 2020.  We will communicate with you once the request is closed.</p>
                        </div>
                        <div className="act-btn">
                        <button type="button" className="btn btn-fill">Back to Policy Details</button>
                        </div>
                    </div>
                </div>
            </div>
            <span className={`overlay ${(ABCidModalState) || (OTPModalState)?'active':''}`}></span>
            {/* <Footer /> */}
            <FloatingMenu />
            <Loader show={loader} />
        </>
    );
}

export default UpdateNominee;
