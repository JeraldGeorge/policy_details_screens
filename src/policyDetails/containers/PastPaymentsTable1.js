import React from 'react'


import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Toolbar from "@material-ui/core/Toolbar";
import Paper from "@material-ui/core/Paper";
import { NavLink } from 'react-router-dom';


const rows = [
    {date: '17/03/2020', amount: '50,000', status: 'In Process', purpose: 'Premium'},
    {date: '17/03/2020', amount: '50,000', status: 'Approved', purpose: 'Premium'},
    {date: '17/03/2020', amount: '50,000', status: 'Approved', purpose: 'Premium'},
    {date: '17/03/2020', amount: '50,000', status: 'Approved', purpose: 'Premium'},
    {date: '17/03/2020', amount: '50,000', status: 'Approved', purpose: 'Premium'},
    {date: '17/03/2020', amount: '50,000', status: 'Failed', purpose: 'Premium'},
    {date: '17/03/2020', amount: '50,000', status: 'In Process', purpose: 'Premium'},
    {date: '17/03/2020', amount: '50,000', status: 'Approved', purpose: 'Premium'},
    {date: '17/03/2020', amount: '50,000', status: 'Approved', purpose: 'Premium'},
    {date: '17/03/2020', amount: '50,000', status: 'Failed', purpose: 'Premium'},
    {date: '17/03/2020', amount: '50,000', status: 'Approved', purpose: 'Premium'},
    {date: '17/03/2020', amount: '50,000', status: 'In Process', purpose: 'Premium'},
   ]

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  {
    id: "date",
    numeric: false,
    disablePadding: true,
    mobileHide: false,
    label: "Date of payment",
  },
  {
    id: "paid",
    numeric: false,
    disablePadding: false,
    mobileHide: false,
    label: "Premium Paid",
  },
  {
    id: "status",
    numeric: false,
    disablePadding: false,
    mobileHide: false,
    label: "Status",
  },
  {
    id: "purpose",
    numeric: false,
    disablePadding: false,
    mobileHide: true,
    label: "Purpose of payment",
  },
];

function EnhancedTableHead(props) {
  const { order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell, index) => (
          <TableCell
            key={index}
            padding={headCell.disablePadding ? "none" : "default"}
            className={headCell.mobileHide ? "hidden-xs" : ""}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}




const PastPaymentsTable = () => {


    // Start function table
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("status");
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [viewMoreFlag, setViewMoreFlag] = React.useState(false);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleChangeRowsPerPage = () => {
    if (viewMoreFlag) {
      setRowsPerPage(5);
      setViewMoreFlag(false);
    } else {
      setRowsPerPage(10);
      setViewMoreFlag(true);
    }
  };

  // end function table


    return (
        <div className="m-table-wrap">
            <Paper className="table">
            <TableContainer>
                <Table 
                aria-label="enhanced table"
                >
                <EnhancedTableHead
                    order={order}
                    orderBy={orderBy}
                    onRequestSort={handleRequestSort}
                />
                <TableBody>
                    {stableSort(rows, getComparator(order, orderBy))
                    .slice(
                        page * rowsPerPage,
                        page * rowsPerPage + rowsPerPage
                    )
                    .map((row, index) => {
                        const labelId = `enhanced-table-checkbox-${index}`;

                        return (
                        <TableRow hover key={index}>
                            <TableCell
                            component="th"
                            id={labelId}
                            scope="row"
                            padding="none"
                            >
                            {row.date}
                            </TableCell>
                            <TableCell>&#8377;{row.amount}</TableCell>
                            <TableCell className={`${(row.status) == 'Approved' ? 'approved' : (row.status) == 'Failed' ? 'failed' : 'process'}`}>{row.status}</TableCell>
                            <TableCell className="hidden-xs">{row.purpose}</TableCell>
                        </TableRow>
                        );
                    })}
                </TableBody>
                </Table>
            </TableContainer>
            </Paper>
            {/* <button className="btn btn-link" onClick={handleChangeRowsPerPage}>
            {viewMoreFlag ? "View Less" : "View More"}
            </button> */}
            <NavLink to="" className="btn btn-link">View More</NavLink>
        </div>
    )
}

export default PastPaymentsTable;
