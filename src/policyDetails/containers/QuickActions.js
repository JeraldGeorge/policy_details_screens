import React, { useState, useEffect, useRef } from 'react'
import { NavLink } from "react-router-dom";

function QuickActions(props) {
    const node = useRef();
    const [notificationInfoState, setNotificationInfoState] = useState(false);
    const [whatsappModalState, setWhatsappModalState] = useState(false);

    useEffect(() => {
        // add when mounted
        document.addEventListener("click", handleClickOutside);
        // return function to be called when unmounted
        return () => {
            document.removeEventListener("click", handleClickOutside);
        };
    }, [])
    const handleClickOutside = (e) => {
        if (node.current.contains(e.target)) {
            // inside click
            return;
        }
        setNotificationInfoState(false);
        // outside click 
    }
    const notificationStateHandler = (e) => {
        e.preventDefault();

        setNotificationInfoState(!notificationInfoState);

    };
    const whatsappModalStateHandler = (e, param) => {
        e.preventDefault();
        if (param === "open") {
            setWhatsappModalState(true);
        } else {
            setWhatsappModalState(false);
        }
    };
    return (
        <>
            <ul className="m-icon-list">
                <li className="item">
                    <NavLink to="" className="icon icon-search" title="search"></NavLink>
                </li>
                {props.whatsappStatus !== "Y" && 
                    <li className="item">
                        <NavLink to="" title="what's app" onClick={(e) => { whatsappModalStateHandler(e, "open"); }} className="icon icon-whats-app"></NavLink>
                    </li>
                }
                <li className="item desktop">
                    <NavLink to="" className="icon icon-call" title="call"></NavLink>
                </li>
                <li className="item desktop">
                    <NavLink to="" className="icon icon-branch-locator" title="branch locator"></NavLink>
                </li>
                <li ref={node} className="item">
                    <NavLink to="" title="notification" className="icon icon-notification" onClick={(e) => { notificationStateHandler(e); }} ></NavLink>
                    <div className={`submenu ${notificationInfoState ? "active" : ""}`}>
                        <div className="m-notification-Info">
                            <button className="btn-cancel btn btn-icon" onClick={(e) => { notificationStateHandler(e); }}>
                                <span className="icon icon-cancel"></span>
                            </button>
                            <div className="info-cont">
                                <h3 className="title">
                                    Your last transaction
                      </h3>
                                <ul className="info-list">
                                    <li className="info-item">
                                        <NavLink to="" title="New address details verified for Term Insurance Policy No. 124346983" >
                                            New address details verified for <strong>Term Insurance Policy No. 124346983</strong>
                                        </NavLink>
                                    </li>
                                    <li className="info-item">
                                        <NavLink to="" title="Request processing. Change should reflect within 04 to 05 working days" >
                                            Request processing. Change should reflect within 04 to 05 working days
                          </NavLink>
                                    </li>
                                    <li className="info-item">
                                        <NavLink to="" title="Please add Purpose of Insurance for your Term Insurance Policy No. 1234346983">
                                            Please add Purpose of Insurance for your <strong> Term Insurance Policy No. 1234346983</strong>. This will help us serve you better in the future.
                          </NavLink>
                                        <NavLink to="" title="Add now" className="info-paynow btn btn-link">
                                            Add now
                          </NavLink>
                                    </li>
                                    <li className="info-item">
                                        <NavLink to="" title="New address details verified for Term Insurance Policy No. 124346983">
                                            New address details verified for{" "}
                                            <strong>Term Insurance Policy No. 124346983</strong>
                                        </NavLink>
                                    </li>
                                    <li className="info-item">
                                        <NavLink
                                            to=""
                                            title="Request processing. Change should reflect within 04 to 05 working days"
                                        >
                                            Request processing. Change should reflect within 04
                                            to 05 working days
                          </NavLink>
                                    </li>
                                    <li className="info-item">
                                        <NavLink
                                            to=""
                                            title="Please add Purpose of Insurance for your Term Insurance Policy No. 1234346983"
                                        >
                                            Please add Purpose of Insurance for your{" "}
                                            <strong>
                                                Term Insurance Policy No. 1234346983
                            </strong>
                            . This will help us serve you better in the future.
                          </NavLink>
                                        <NavLink
                                            to=""
                                            title="Add now"
                                            className="info-paynow btn btn-link"
                                        >
                                            Add now
                          </NavLink>
                                    </li>
                                    <li className="info-item">
                                        <NavLink
                                            to=""
                                            title="New address details verified for Term Insurance Policy No. 124346983"
                                        >
                                            New address details verified for{" "}
                                            <strong>Term Insurance Policy No. 124346983</strong>
                                        </NavLink>
                                    </li>
                                    <li className="info-item">
                                        <NavLink
                                            to=""
                                            title="Request processing. Change should reflect within 04 to 05 working days"
                                        >
                                            Request processing. Change should reflect within 04
                                            to 05 working days
                          </NavLink>
                                    </li>
                                    <li className="info-item">
                                        <NavLink
                                            to=""
                                            title="Please add Purpose of Insurance for your Term Insurance Policy No. 1234346983"
                                        >
                                            Please add Purpose of Insurance for your{" "}
                                            <strong>
                                                Term Insurance Policy No. 1234346983
                            </strong>
                            . This will help us serve you better in the future.
                          </NavLink>
                                        <NavLink
                                            to=""
                                            title="Add now"
                                            className="info-paynow btn btn-link"
                                        >
                                            Add now
                          </NavLink>
                                    </li>
                                </ul>
                            </div>
                            <NavLink
                                to=""
                                title="View all"
                                className="info-viewall btn btn-primary"
                            >
                                View all
                    </NavLink>
                        </div>
                    </div>
                </li>
            </ul>
            <div className={`modal ${whatsappModalState ? "active" : ""}`}>
                <div className="dialog">
                    <div className="content">
                        <div className="modal-header">
                            <h5 className="title">Welcome back Kabir Roy</h5>
                            <h5 className="subtitle">
                                Help us to know you better by providing following details
              </h5>
                            <button
                                type="button"
                                onClick={(e) => {
                                    whatsappModalStateHandler(e, "");
                                }}
                                className="btn btn-icon"
                            >
                                <span className="icon icon-cancel"></span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <div className="whatsapp">
                                <div className="checkbox">
                                    <input type="checkbox" id="a" />
                                    <label className="lbl" htmlFor="a">
                                        WhatsApp Opt-in
                  </label>
                                </div>
                                <p>
                                    Enabling WhatsApp Opt-in will allow you to receive important
                                    information & updates over WhatsApp
                </p>
                                <span className="note">
                                    <span className="text">Your registered mobile number</span>
                                    <span className="num">98300 98330</span>
                                </span>
                            </div>
                        </div>
                        <div className="act-btn">
                            <button
                                type="button"
                                onClick={(e) => {
                                    whatsappModalStateHandler(e, "");
                                }}
                                className="btn btn-link"
                            >
                                Skip this for now
              </button>
                            <button type="button" className="btn">
                                Save
              </button>
                        </div>
                    </div>
                </div>
            </div>
            <span className={`overlay ${whatsappModalState ? "active" : ""}`}></span>
        </>
    )
}

export default QuickActions
