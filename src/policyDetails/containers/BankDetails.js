import React, { useEffect, useState } from "react";
import { isMobile } from "react-device-detect";
// import Header from "../components/Header";
// import Footer from "../components/Footer";
import SideNav from "../../home/containers/sideNav";
import FloatingMenu from "./FloatingMenu";
import Loader from "../../home/containers/loader";
import { NavLink } from "react-router-dom";
import QuickActions from "./QuickActions";
import PolicyCardBankDetail from "./PolicyCardBankDetail";
import PolicyCardBankDetail1 from "./PolicyCardBankDetail1";

function BankDetails() {
    const [loader, setLoader] = useState(true);
    const [sidebarMobileState, setSidebarMobileState] = useState(false);

    useEffect(() => {
        setLoader(false);
    }, []);


    const sideBarHandlerMobile = () => {
        if (sidebarMobileState) {
            setSidebarMobileState(false);
        } else {
            setSidebarMobileState(true);
        }
    };

    return (
        <>
            {/* <Header /> */}
            <div className="container-fluid refContainer">
                <SideNav openStateMobile={sidebarMobileState} />
                <div className="content-wrapper">
                    <div className="lyt-bank-details">
                        {/* action header start */}
                        <div className="bs-action-header">
                            {isMobile ? (
                                <h1 className="title" onClick={sideBarHandlerMobile}>
                                    Bank Account Details
                                    <span className="icon icon-chevron-down"></span>
                                </h1>
                            ) : (
                                    <h1 className="title">
                                        Bank Account Details
                                        <span className="icon icon-chevron-down"></span>
                                    </h1>
                                )}
                            <QuickActions />
                        </div>
                        {/* action header end */}
                        {/* breadcrumb starts */}
                        <ol className="m-breadcrumb desktop">
                            <li className="item">
                                <NavLink to="/dashboard" className="link" title="Home">
                                    Home<span className="icon icon-chevron-right"></span>
                                </NavLink>
                            </li>
                            <li className="item">
                                <NavLink to="" className="link" title="Home">
                                    Policies Related Requests<span className="icon icon-chevron-right"></span>
                                </NavLink>
                            </li>
                            <li className="item">Bank Account Details</li>
                        </ol>
                        {/* breadcrumb end */}
                        {/* page template start here */}
                        <div className="bs-section">
                            <div className="sec-head">
                            <p className="sec-msg">
                                The account details you add are used for disbursement of survival benefits / annuity / maturity payout.
                                </p>
                            </div>
                             <div className="sec-cont">
                                <PolicyCardBankDetail/> 
                                <PolicyCardBankDetail/>
                                <PolicyCardBankDetail1/>
                             </div>
                        </div>


                        {/* page template end here */}
                    </div>
                </div>
            </div>
            {/* <Footer /> */}

            <FloatingMenu />
            <Loader show={loader} />

        </>
    );
}

export default BankDetails;
