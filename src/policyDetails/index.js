import React, { useState, useEffect } from 'react';
import PolicyListing from './components/PolicyListing';
import PolicyDetail from './components/PolicyDetail';
import {connect} from 'react-redux';

 const PolicyScreens = (props) => {
    const [policyData, setPolicyData] = useState(true);
    const [policyDetail, setPolicyDetail] = useState('');
    const [dashboardData, setdashboardData] = useState('');
    const {dashboard} = props;

    useEffect(() => {
        // setPolicyData(true);
        setdashboardData(dashboard)
    },[dashboard])

    const buildPolicyScreens = () => {
        if(policyData){
            return <PolicyListing
                handleRouting={handleRouting}
                dashboardData={dashboardData}
                // {...props}
            />
        }else{
            return <PolicyDetail
                handleBack={handleBack}
                policyDetail={policyDetail}
                {...props}
            />
        }
    }

    const handleBack = () => {
        console.log('handleBack called')
        setPolicyData(true);
        setPolicyDetail('');
    }

    const handleRouting = (data) => {
        console.log('handleRouting in index',data)
        setPolicyData(false);
        setPolicyDetail(data)
    }



    return(
        <>
            {buildPolicyScreens()}
        </>
    )
} 

const mapStatetoProps = (state) => {
    return{
        dashboard : state.home.dashboard
    }
}

export default connect(mapStatetoProps)(PolicyScreens);