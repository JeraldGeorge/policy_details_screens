import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import App from './home/components/App';
import { store,persistor } from './config/redux/appStore';
import './sass/app.scss';

//import * as serviceWorker from './serviceWorker';

const renderApp = () =>
render(
  <React.Fragment>
   <Provider store = { store } >
           <PersistGate persistor={ persistor }>
               <App />
           </PersistGate>
      </Provider> 
  </React.Fragment>,
  document.getElementById('root')
);

if (process.env.NODE_ENV !== 'production' && module.hot) {
  module.hot.accept('./home/components/App', renderApp)
}

renderApp()
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
//serviceWorker.unregister();
