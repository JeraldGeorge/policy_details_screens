import React from "react";
import "./indexlist.css";

function IndexList() {
  return (
    <>
      <img
        class="logo"
        src="/assets/images/abc-logo.png"
        alt="ABG Customer Portal"
      />
      <div className="siteContainer">
        <h1 className="title">Pages</h1>{" "}
        <a href="/components" className="comp">
          View Components
        </a>
        <ul className="custAcc">
          <li className="opn">
            <div className="lbl">
              <a href="#">All Login Journey</a>
              <span className="icn"></span>
            </div>
            <ul className="subMenu">
              <li className="opn">
                <div className="lbl">
                  <a href="#">Register</a>
                  <span className="icn"></span>
                </div>
                <ul className="subMenu">
                  <li>
                    <a href="/register">Register</a>
                    <span class="icn"></span>
                  </li>
                  <li>
                    <a href="/registerotp">OTP</a>
                    <span class="icn"></span>
                  </li>
                </ul>
              </li>
              <li className="opn">
                <div className="lbl">
                  <a href="#">Login with Password</a>
                  <span className="icn"></span>
                </div>
                <ul className="subMenu">
                <li>
                    <a href="/login-userid">Login with User ID & Popup For ABSLI ID</a>
                    <span class="icn"></span>
                  </li>
                  <li>
                    <a href="/login">Login with Password</a>
                    <span class="icn"></span>
                  </li>
                </ul>
              </li>
              <li className="opn">
                <div className="lbl">
                  <a href="#">Login with OTP</a>
                  <span className="icn"></span>
                </div>
                <ul className="subMenu">
                  <li>
                    <a href="/loginwithotp">Login with OTP</a>
                    <span class="icn"></span>
                  </li>
                  <li>
                    <a href="/registerotp">OTP</a>
                    <span class="icn"></span>
                  </li>
                </ul>
              </li>
              <li className="opn">
                <div className="lbl">
                  <a href="#">Login with Quick Link</a>
                  <span className="icn"></span>
                </div>
                <ul className="subMenu">
                  <li>
                    <a href="/quicklink">Quick Link</a>
                    <span class="icn"></span>
                  </li>
                  <li>
                    <a href="/registerotp">OTP</a>
                    <span class="icn"></span>
                  </li>
                </ul>
              </li>
              <li className="opn">
                <div className="lbl">
                  <a href="#">Forgot User ID</a>
                  <span className="icn"></span>
                </div>
                <ul className="subMenu">
                  <li>
                    <a href="/forgotuserid">Forgot User ID</a>
                    <span class="icn"></span>
                  </li>
                  <li>
                    <a href="/registerotp">OTP</a>
                    <span class="icn"></span>
                  </li>
                </ul>
              </li>
              <li className="opn">
                <div className="lbl">
                  <a href="#">Forgot Password</a>
                  <span className="icn"></span>
                </div>
                <ul className="subMenu">
                  <li>
                    <a href="/forgotpassword">Forgot Password</a>
                    <span class="icn"></span>
                  </li>
                  <li>
                    <a href="/registerotp">OTP</a>
                    <span class="icn"></span>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li className="opn">
            <div className="lbl">
              <a href="#">Dashboard</a>
              <span className="icn"></span>
            </div>
            <ul className="subMenu">
              <li className="opn">
                <div className="lbl">
                  <a href="/dashboard">Dashboard Page</a>
                  <span className="icn"></span>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </>
  );
}

export default IndexList;
