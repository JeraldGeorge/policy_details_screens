import { combineReducers } from 'redux';
import HomeReducer from '../../reducers/home';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
//import DashReducer from '../../reducers/dashboard';

const persistConfig = {
key : 'root',
storage,
whitelist :['home']
}

const rootReducer = combineReducers({
    home : HomeReducer,
   // dashBoard : DashReducer
});

export default persistReducer(persistConfig,rootReducer);