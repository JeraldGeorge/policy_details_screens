import { createStore,applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { persistStore } from 'redux-persist';
import rootReducer from './rootReducer';
import thunk from 'redux-thunk';



const middlewares = [thunk];

const enhancer = composeWithDevTools(applyMiddleware(...middlewares));

export const store = createStore(
rootReducer,    
enhancer
);

if (process.env.NODE_ENV !== 'production' && module.hot) {
    module.hot.accept('./rootReducer', () => store.replaceReducer(rootReducer))
  }

export const persistor=persistStore(store);

export default { store,persistor };