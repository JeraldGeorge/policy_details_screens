import {useEffect,useState} from 'react'

function useGetPositions(containerClassName) {
    let containerObj;

    const resizeContainer =() => {
        containerObj = document.getElementsByClassName(containerClassName)[0];
        let rect = containerObj.getBoundingClientRect(),
        scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
        scrollTop = window.pageYOffset || document.documentElement.scrollTop,
        rightPos = window.innerWidth - (rect.left + scrollLeft+rect.width)
        setPos( { top: rect.top + scrollTop, left: rect.left + scrollLeft,width:rect.width,height:rect.height,right:rightPos })
    }
    
    const [pos,setPos] = useState({})
    useEffect(()=>{
        containerObj = document.getElementsByClassName(containerClassName)[0];
        
        let rect = containerObj.getBoundingClientRect(),
        scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
        scrollTop = window.pageYOffset || document.documentElement.scrollTop,
        rightPos = window.innerWidth - (rect.left + scrollLeft+rect.width)
        setPos( { top: rect.top + scrollTop, left: rect.left + scrollLeft,width:rect.width,height:rect.height,right:rightPos })

        window.addEventListener("resize",resizeContainer)
        return ()=> {
           window.removeEventListener("resize",resizeContainer);
        }
       
    },[])
    
    return [pos];
}

export default useGetPositions
