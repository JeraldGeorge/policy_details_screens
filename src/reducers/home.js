import * as actionTypes from '../constant/actionTypes'; 

// const initialState = {
//     errorPopup : false

// };


const  reducer  = (state = {} ,action ) => {

     switch(action.type) {
      
     case actionTypes.INIT_LOADER :
        return Object.assign({}, state, {
            loader: true
        });
     case actionTypes.REMOVE_LOADER :
        delete state.loader;
        return Object.assign({}, state);       
     case actionTypes.AUTHENTICATE :
         return Object.assign({}, state, {
             users: action.user
           }); 
     case actionTypes.LOGIN_URL :
         return Object.assign({},state,{
            path: action.pathUrl 
         });
     case actionTypes.DASHBOARD_DETAILS :
         return Object.assign({}, state, {
                dashboard: action.dashBoard
              }); 
     case actionTypes.NETWORK_ISSUE :
        return Object.assign({}, state, {
                       errorPopup: action.errorPopup
                     }); 
    case actionTypes.LOG_OUT :
         state={}                
       default :
       return state ;

   }
};

export default reducer;