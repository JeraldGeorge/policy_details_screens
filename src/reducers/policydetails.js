// import * as actionTypes from '../actions/policydetails';
import * as actionTypes from '../constant/actionTypes';

const policydetailsReducer = (state = {}, action) => {
    // console.log('policydetailsReducer called');
    switch(action.type){
        case actionTypes.POLICY_DETAILS :
        return Object.assign({}, state, {
                policydetails: action.policydetails
            }); 
        default :
        return state;    
    }
}

export default policydetailsReducer;