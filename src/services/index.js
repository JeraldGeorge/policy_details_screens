import axios from '../utils/axios-interceptor';
import setting from '../config/settings';



export async function chkUser (data)
{
    const postData = { "api" :"checkCommonUser","ABCUserID" : data };
    try {
    const chkuserData=await axios.post('/login',postData);
    if(chkuserData)
    {
    return chkuserData.data;
    }
   }catch(err) {
    const ErrorData = {"error" : 'Network Issue' }     
    return ErrorData;
    }
}

export async function sendOtp(data)
{
    try{
        const sendOtp = await axios.post("/login",data);
        const resData=sendOtp.data;

        if(resData.data && resData.status===10)
        {
            return resData;
        }
        else {
            return resData;
        }

    }catch(err){
        const ErrorData = {"error" : 'Network Issue' }     
        return ErrorData;
    }

}

export async function verifyOtp(data)
{
    try{
        const verifyOtp=await axios.post("/login",data)
        if(verifyOtp.data)
        {
            return verifyOtp.data;
        }
    }catch(err){
        const ErrorData = {"error" : 'Network Issue' }     
       return ErrorData;
    }
}

export async function otpLogin(data)
{
   try{
    const postData = await axios.post('/login',data);
    const responseData= postData.data;
    if(responseData && responseData.status===10)
    {
        return responseData;
    }
   }catch(err)
   {
    const ErrorData = {"error" : 'Network Issue' }     
    return ErrorData;
   }
}

export async function abcLogin(data)
{
    try
    {
        const userData=await axios.post("/login",data);
        const resData=userData.data;
        if(resData)
        {
            return resData;
        }

        // if(resData.data && (resData.data.ReturnCode==="2" || resData.data.ReturnCode==="4"))
        // {
        //     return resData;
        // }
    //     }else{



    //         //console.log('prasad');
    //     //     if(resData.data.ReturnCode==="1")
    //     //     {
    //     //        const paraMeters={
    //     //                         "TokenNo" : resData.data.TokenNo,
    //     //                         "TokenRefNo" : resData.data.TokenRefNo,
    //     //                         "CIINo" : resData.data.CIINo 
    //     //                         };
    //     //      const response= redirectLogin(paraMeters);
    //     //      return response;          
    //     // }
    // }

    }catch(error)
    {
        const ErrorData = {"error" : 'Network Issue' }     
       return ErrorData;
    }
}

export async function redirectLogin(data)
{
    try{
  axios.post(setting.UDT_ABC_LOGIN_URL,data).then().catch(err =>{
    return {"server_err" : "Cant Able to Communicate with server"}
  });
 
}catch(err)
{
    const ErrorData = {"error" : 'Network Issue' }     
    return ErrorData;

}  
}

export async function getdashDetails()
{
    try{
        const serviceRequest = await axios.post('/getDashboardDetails');
        const reqData=serviceRequest.data;
        if(reqData.status ===10 && reqData.data)
        {
            return reqData.data;
     }
    }catch(err)
    {
        const ErrorData = {"error" : 'Network Issue' }     
        return ErrorData;
    }
}

export async function registerDetails(data)
{
   try
   {
       const serviceRequest = await axios.post('/userRegistration',data);
       const reqData = serviceRequest.data;
       if(reqData.status ===10 && reqData.data)
       {
           return reqData;
       }else {
           return reqData.message;
       }
   }catch(err)
    {
        const ErrorData = {"error" : 'Network Issue' }     
        return ErrorData;
    }
}

export async function getPolicyDetails(){
    // console.log('getPolicyDetails called in service',c)
    let postData =  {};
    postData = JSON.parse(localStorage.getItem('test'))
    console.log('postData',postData)
    try{
        // console.log('getPolicyDetails called in service',data)
        const serviceRequest = await axios.post('/getPolicyDetails',postData);
        const reqData = serviceRequest.data;
        if(reqData.status ===10 && reqData.data)
        {
            console.log('reqData',reqData)
            return reqData.data;
        }
        return reqData.data;
    }catch(err){
        const ErrorData = {"error" : 'Issue'}
        return ErrorData;
    }
}